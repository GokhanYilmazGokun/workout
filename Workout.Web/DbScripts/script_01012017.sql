USE [master]
GO
/****** Object:  Database [Workout]    Script Date: 1.01.2017 20:36:03 ******/
CREATE DATABASE [Workout]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Workout', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Workout.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Workout_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Workout_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Workout] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Workout].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Workout] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Workout] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Workout] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Workout] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Workout] SET ARITHABORT OFF 
GO
ALTER DATABASE [Workout] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Workout] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Workout] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Workout] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Workout] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Workout] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Workout] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Workout] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Workout] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Workout] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Workout] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Workout] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Workout] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Workout] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Workout] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Workout] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Workout] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Workout] SET RECOVERY FULL 
GO
ALTER DATABASE [Workout] SET  MULTI_USER 
GO
ALTER DATABASE [Workout] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Workout] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Workout] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Workout] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Workout] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Workout', N'ON'
GO
USE [Workout]
GO
/****** Object:  Table [dbo].[Certificates]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Certificates](
	[ID] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[CertificateNumber] [nvarchar](200) NOT NULL,
	[CertificateName] [nvarchar](200) NOT NULL,
	[ExpireDate] [datetime] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
 CONSTRAINT [PK_Certificates_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Comments]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[ID] [uniqueidentifier] NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[WorkoutID] [uniqueidentifier] NULL,
	[LikeCount] [int] NULL,
	[ProgramID] [uniqueidentifier] NULL,
	[IsActive] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Comments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CoreUsers]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CoreUsers](
	[ID] [uniqueidentifier] NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CoreUsers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ErrorLogs]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ErrorLogs](
	[Id] [uniqueidentifier] NOT NULL,
	[ErrorMessage] [nvarchar](max) NULL,
	[InnerException] [nvarchar](max) NULL,
	[Url] [nvarchar](max) NULL,
	[Page] [nvarchar](max) NULL,
	[Ip] [nvarchar](50) NULL,
	[CreateDate] [datetime] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_ErrorLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Programs]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Programs](
	[ID] [uniqueidentifier] NOT NULL,
	[Rate] [float] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Subtitle] [nvarchar](200) NOT NULL,
	[Information] [nvarchar](max) NOT NULL,
	[ViewCount] [int] NOT NULL CONSTRAINT [DF_Programs_ViewCount]  DEFAULT ((0)),
	[AverageWeightLoss] [float] NOT NULL,
	[IsEditable] [tinyint] NOT NULL CONSTRAINT [DF_Programs_IsEditable]  DEFAULT ((1)),
	[IsDeleted] [tinyint] NOT NULL CONSTRAINT [DF_Programs_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_Program] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PWRelation]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PWRelation](
	[ID] [uniqueidentifier] NOT NULL,
	[ProgramID] [uniqueidentifier] NOT NULL,
	[WorkoutID] [uniqueidentifier] NOT NULL,
	[Queue] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[Period] [int] NOT NULL,
	[Repeat] [int] NOT NULL,
 CONSTRAINT [PK_PWRelation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rates]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rates](
	[ID] [uniqueidentifier] NOT NULL,
	[Five] [int] NOT NULL,
	[Four] [int] NOT NULL,
	[Three] [int] NOT NULL,
	[Two] [int] NOT NULL,
	[One] [int] NOT NULL,
 CONSTRAINT [PK_Rates] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sessions]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sessions](
	[ID] [uniqueidentifier] NOT NULL,
	[LoginDate] [datetime] NOT NULL,
	[LogoutDate] [datetime] NULL,
	[UserAgent] [nvarchar](500) NOT NULL,
	[IP] [nvarchar](50) NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Sessions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UPRelation]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UPRelation](
	[ID] [uniqueidentifier] NOT NULL,
	[ProgramID] [uniqueidentifier] NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[RegisterDate] [datetime] NOT NULL,
 CONSTRAINT [PK_UPRelation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [uniqueidentifier] NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](200) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[Birthdate] [datetime] NOT NULL,
	[Gender] [tinyint] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[Role] [int] NOT NULL,
	[Phone] [nvarchar](50) NULL,
	[Height] [float] NOT NULL,
	[Weight] [float] NOT NULL,
	[ViewCount] [int] NOT NULL CONSTRAINT [DF_Users_ViewCount]  DEFAULT ((0)),
	[SecurityQuestion] [nvarchar](max) NULL,
	[SecurityAnswer] [nvarchar](max) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserWeightChangeLog]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserWeightChangeLog](
	[ID] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[WeightLoss] [float] NOT NULL,
	[RecordDate] [datetime] NOT NULL,
	[ProgramID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_UserWeightChangeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Workouts]    Script Date: 1.01.2017 20:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workouts](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Video] [nvarchar](250) NOT NULL,
	[Information] [nvarchar](max) NOT NULL,
	[Rate] [float] NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Subtitle] [nvarchar](200) NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[Category] [int] NOT NULL,
	[WClass] [int] NOT NULL,
 CONSTRAINT [PK_Workouts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Comments] ADD  CONSTRAINT [DF_Comments_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Certificates]  WITH CHECK ADD  CONSTRAINT [FK_Certificates_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[Certificates] CHECK CONSTRAINT [FK_Certificates_Users]
GO
ALTER TABLE [dbo].[Comments]  WITH NOCHECK ADD  CONSTRAINT [FK_Comments_Programs] FOREIGN KEY([ProgramID])
REFERENCES [dbo].[Programs] ([ID])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Comments] NOCHECK CONSTRAINT [FK_Comments_Programs]
GO
ALTER TABLE [dbo].[Comments]  WITH NOCHECK ADD  CONSTRAINT [FK_Comments_Workouts] FOREIGN KEY([WorkoutID])
REFERENCES [dbo].[Workouts] ([ID])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Comments] NOCHECK CONSTRAINT [FK_Comments_Workouts]
GO
ALTER TABLE [dbo].[PWRelation]  WITH NOCHECK ADD  CONSTRAINT [FK_PWRelation_Programs] FOREIGN KEY([ProgramID])
REFERENCES [dbo].[Programs] ([ID])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PWRelation] NOCHECK CONSTRAINT [FK_PWRelation_Programs]
GO
ALTER TABLE [dbo].[PWRelation]  WITH NOCHECK ADD  CONSTRAINT [FK_PWRelation_Workouts] FOREIGN KEY([WorkoutID])
REFERENCES [dbo].[Workouts] ([ID])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PWRelation] NOCHECK CONSTRAINT [FK_PWRelation_Workouts]
GO
ALTER TABLE [dbo].[UPRelation]  WITH NOCHECK ADD  CONSTRAINT [FK_UPRelation_Programs] FOREIGN KEY([ProgramID])
REFERENCES [dbo].[Programs] ([ID])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[UPRelation] NOCHECK CONSTRAINT [FK_UPRelation_Programs]
GO
ALTER TABLE [dbo].[UPRelation]  WITH NOCHECK ADD  CONSTRAINT [FK_UPRelation_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[UPRelation] NOCHECK CONSTRAINT [FK_UPRelation_Users]
GO
USE [master]
GO
ALTER DATABASE [Workout] SET  READ_WRITE 
GO
