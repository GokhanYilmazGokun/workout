﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workout.Business.Models.Enums;
using Workout.Business.Models.RequestModels;
using Workout.Business.Processes;
using Workout.Data;
using Workout.Data.Processes;

namespace Workout.Web.Controllers
{
    public class AdminController : Controller
    {
        #region Decleration
        private WorkoutProcesses workoutProcesses = new WorkoutProcesses();
        private UserProcesses userProcesses = new UserProcesses();
        #endregion


        #region Error Handling
        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            LogProcesses.ErrorLog(ex);
        }
        #endregion

        [HttpPost]
        public ActionResult DoLogin(string username, string password, string url)
        {
            var sessionHelper = new SessionProcesses();
            var response = sessionHelper.GetAdminLoginResponse(username, password);

            if (response.ResponseCode == 1 || response.ResponseCode == 2)
            {
                if (string.IsNullOrEmpty(url))
                    return RedirectToAction("RegisterRequests");
                else
                    return RedirectToAction(url);
            }
            else
                return RedirectToAction("Login", new { url = "Login", validation = false });

        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Logout()
        {
            if (HttpContext.Request.Cookies["SIDA"] != null)
            {
                var sessionHelper = new SessionProcesses();
                sessionHelper.GetAdminLogoutResponse(HttpContext.Request.Cookies["SIDA"].Value);
                return RedirectToAction("RegisterRequests");
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        [HttpGet]
        public ActionResult ErrorPage()
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                return View();
            }
            else
                return RedirectToAction("Login", new { url = "ErrorPage" });
        }

        [HttpGet]
        public ActionResult TrainerCertificates(string username = null)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {
                var certificates = new List<Certificates>();
                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                var users = userProcesses.GetDeactiveProfiles();
                ViewBag.UserList = users.UserList;
                if (!string.IsNullOrEmpty(username))
                {
                    var userId = userProcesses.GetUserIdByUsername(username);
                    certificates = userProcesses.GetCertificatesForUser(userId);
                }
                else
                {
                    certificates = userProcesses.GetCertificatesForAllUsers();
                }
                ViewBag.Certificates = certificates;
                return View();
            }
            else
                return RedirectToAction("Login", new { url = "RegisterRequests" });
        }

        [HttpGet]
        public ActionResult TrainerDeactivate(string username = null)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {

                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                List<Users> users = new List<Users>();

                if (!string.IsNullOrEmpty(username))
                {
                    users.Add(userProcesses.GetTrainerByUsername(username).FirstOrDefault());
                }

                ViewBag.UserList = users;
                return View();
            }
            else
                return RedirectToAction("Login", new { url = "RegisterRequests" });
        }

        public ActionResult BanTrainer(Guid userId)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {

                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());

                if (userId != Guid.Empty)
                {
                    if (userProcesses.BanTrainer(userId))
                        return RedirectToAction("TrainerDeactivate");

                }
                return RedirectToAction("TrainerDeactivate");
            }
            else
                return RedirectToAction("Login", new { url = "RegisterRequests" });
        }

        [HttpGet]
        public ActionResult ProgramDeactivate(string programTitle = null)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {

                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                List<Programs> programs = new List<Programs>();

                if (!string.IsNullOrEmpty(programTitle))
                {
                    programs = workoutProcesses.GetProgramsWithTitle(programTitle, 0);
                }

                ViewBag.ProgramList = programs;
                return View();
            }
            else
                return RedirectToAction("Login", new { url = "RegisterRequests" });
        }

        public ActionResult BanProgram(Guid programId)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {

                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());

                if (programId != Guid.Empty)
                {
                    if (userProcesses.BanProgram(programId))
                        return RedirectToAction("ProgramDeactivate");

                }
                return RedirectToAction("ProgramDeactivate");
            }
            else
                return RedirectToAction("Login", new { url = "RegisterRequests" });
        }

        public ActionResult ActivateBannedProgram(Guid programId)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {

                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());

                if (programId != Guid.Empty)
                {
                    if (userProcesses.ActivateBannedProgram(programId))
                        return RedirectToAction("ProgramDeactivate");

                }
                return RedirectToAction("ProgramDeactivate");
            }
            else
                return RedirectToAction("Login", new { url = "RegisterRequests" });
        }

        [HttpGet]
        public ActionResult UserDeactivate(string username = null)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {

                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                List<Users> users = new List<Users>();

                if (!string.IsNullOrEmpty(username))
                {
                    users.Add(userProcesses.GetUserByUsername(username));
                }

                ViewBag.UserList = users;
                return View();
            }
            else
                return RedirectToAction("Login", new { url = "RegisterRequests" });
        }

        public ActionResult BanUser(Guid userId)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {
                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());

                if (userId != Guid.Empty)
                {
                    if (userProcesses.BanUser(userId))
                        return RedirectToAction("UserDeactivate");

                }
                return RedirectToAction("UserDeactivate");
            }
            else
                return RedirectToAction("Login", new { url = "RegisterRequests" });
        }

        [HttpGet]
        private void ViewBagSet(string username, string userId)
        {
            ViewBag.Username = username.ToUpper();
            ViewBag.UserId = userId;
        }
        [HttpPost]
        public JsonResult DeleteComment()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<DeleteCommentRequest>(ReadJson());
                return Json(workoutProcesses.DeleteComment(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public ActionResult RegisterRequests()
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {
                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                var users = userProcesses.GetDeactiveProfiles();
                ViewBag.UserList = users.UserList;
                return View();
            }
            else
                return RedirectToAction("Login", new { url = "RegisterRequests" });
        }

        public ActionResult ActivateUser(Guid userID)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {
                userProcesses.ActivateUser(userID);
                return RedirectToAction("RegisterRequests", "Admin");
            }
            else
                return RedirectToAction("Login", new { url = "RegisterRequests" });
        }


        [HttpGet]
        public ActionResult Trainers(string username)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {
                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                if (!string.IsNullOrEmpty(username))
                    ViewBag.Trainers = userProcesses.GetTrainerByUsername(username);
                else
                    ViewBag.Trainers = userProcesses.GetAllTrainers();

                ViewBag.Counts = userProcesses.GetTrainersProgramAndCommentCounts(username);
                return View();

            }
            return View();
        }

        [HttpGet]
        public ActionResult TrainerStatistics(Guid trainerId, DateTime? startDate, DateTime? endDate, Guid? programId)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {

                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                if (startDate == null && endDate == null && programId == null)
                {
                    ViewBag.ProgramsOfTrainer = workoutProcesses.GetAllprogramsForTrainerByIdAdmin(trainerId).Program;
                }
                else if (startDate != null && endDate != null && programId != null)
                {
                    var programs = workoutProcesses.GetAllprogramsForTrainerByIdAdmin(trainerId).Program;
                    ViewBag.ProgramsOfTrainer = programs.Where(_ => _.Id == programId && (_.CreateDate >= startDate && _.CreateDate <= endDate)).ToList();
                }
                else if (programId != null)
                {
                    var programs = workoutProcesses.GetAllprogramsForTrainerByIdAdmin(trainerId).Program;
                    ViewBag.ProgramsOfTrainer = programs.Where(_ => _.Id == programId).ToList();
                }
                else
                {
                    var programs = workoutProcesses.GetAllprogramsForTrainerByIdAdmin(trainerId).Program;
                    ViewBag.ProgramsOfTrainer = programs.Where(_ => _.CreateDate >= startDate && _.CreateDate <= endDate).ToList();
                }

                ViewBag.RegisteredUsers = userProcesses.GetRegisteredUsersOfProgram(trainerId);

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpGet]
        public ActionResult RegisteredUsers(Guid programId)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {

                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                ViewBag.RegisteredUsers = workoutProcesses.GetRegisteredUsersOfProgramByAdmin(programId);
                ViewBag.ProgramId = programId;

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult WeightLogOfUser(Guid programId, Guid userId)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = sessionHelper.GetCurrentUser(UserRole.Admin);
            if (currentUser != null)
            {

                ViewBag.User = currentUser;
                ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());

                var weightLog = userProcesses.GetWeightLog(userId, programId);

                if (weightLog == null)
                {
                    weightLog = new UserWeightChangeLog();
                }

                ViewBag.WeightLog = weightLog;

                return View();

            }
            return RedirectToAction("Index", "Home");
        }

        #region Common Functions
        private string ReadJson()
        {
            try
            {
                JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { DateFormatString = "dd-MM-yyyy" };
                Request.InputStream.Position = 0;

                using (var inputStream = new StreamReader(Request.InputStream))
                {
                    return inputStream.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}