﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Workout.Business.Models.Enums;
using Workout.Business.Models.RequestModels;
using Workout.Business.Processes;
using Workout.Data;
using Workout.Web.Helpers;

namespace Workout.Web.Controllers
{
    public class HomeController : Controller
    {
        #region Decleration
        private WorkoutProcesses workoutProcesses = new WorkoutProcesses();
        private UserProcesses userProcesses = new UserProcesses();
        private string jsonString = String.Empty;
        #endregion

        #region Views

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpGet]
        public ActionResult SecurityControl(bool? result)
        {
            ViewBag.Result = result;
            return View();
        }
        [HttpPost]
        public ActionResult SecurityCheck(string email)
        {
            if (!string.IsNullOrEmpty(email))
            {
                var securityQuestion = userProcesses.GetSecurityQuestion(email);
                if (!string.IsNullOrEmpty(securityQuestion))
                {
                    ViewBag.Question = securityQuestion;
                    return View();
                }
                return RedirectToAction("SecurityControl", "Home", new { result = false });
            }
            else
            {
                return RedirectToAction("SecurityControl", "Home", new { result = false });
            }
        }

        [HttpGet]
        public ActionResult TrainerProfile()
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    return View();
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
        }
        [HttpGet]
        public ActionResult Profile()
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 1)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    return View();
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult NewPassword(string question, string answer)
        {
            if (!string.IsNullOrEmpty(question) && !string.IsNullOrEmpty(answer))
            {
                if (userProcesses.SecurityAnswerQuestionCheck(question, answer))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("SecurityControl", "Home", new { result = false });
                }
            }
            else
            {
                return RedirectToAction("SecurityControl", "Home", new { result = false });
            }
        }
        [HttpPost]
        public ActionResult SaveNewPassword(string password, string passwordRepeat)
        {
            if (!string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(passwordRepeat))
            {
                if (passwordRepeat == password)
                {
                    if (userProcesses.SetNewPasswordToUser(password))
                        return RedirectToAction("SecurityControl", "Home", new { result = true });
                    else
                        return RedirectToAction("SecurityControl", "Home", new { result = false });
                }
                else
                {
                    return RedirectToAction("SecurityControl", "Home", new { result = false });
                }
            }
            else
            {
                return RedirectToAction("SecurityControl", "Home", new { result = false });
            }
        }
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Index()
        {

            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 1)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.MyPrograms = userProcesses.GetMyPrograms(currentUser.ID);
                    return View();
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("TrainerPanel", "Home");
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult Trainers(string username)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 1)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    if (!string.IsNullOrEmpty(username))
                        ViewBag.Trainers = userProcesses.GetTrainerByUsername(username);
                   

                    ViewBag.Counts = userProcesses.GetTrainersProgramAndCommentCounts(username);
                    return View();
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("TrainerPanel", "Home");
                }
            }
            return View();
        }


        [HttpGet]
        public ActionResult TrainerStatistics(Guid trainerId, DateTime? startDate, DateTime? endDate, Guid? programId)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 1)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());

                    if (startDate == null && endDate == null && programId == null)
                    {
                        ViewBag.ProgramsOfTrainer = workoutProcesses.GetAllprogramsForTrainerById(trainerId).Program;
                    }
                    else if (startDate != null && endDate != null && programId != null)
                    {
                        var programs = workoutProcesses.GetAllprogramsForTrainerById(trainerId).Program;
                        ViewBag.ProgramsOfTrainer = programs.Where(_ => _.Id == programId && (_.CreateDate >= startDate && _.CreateDate <= endDate)).ToList();
                    }
                    else if (programId != null)
                    {
                        var programs = workoutProcesses.GetAllprogramsForTrainerById(trainerId).Program;
                        ViewBag.ProgramsOfTrainer = programs.Where(_ => _.Id == programId).ToList();
                    }
                    else
                    {
                        var programs = workoutProcesses.GetAllprogramsForTrainerById(trainerId).Program;
                        ViewBag.ProgramsOfTrainer = programs.Where(_ => _.CreateDate >= startDate && _.CreateDate <= endDate).ToList();
                    }
                    
                    ViewBag.RegisteredUsers = userProcesses.GetRegisteredUsersOfProgram(trainerId);
                    ViewBag.CurrentUser = currentUser;
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("TrainerPanel", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpGet]
        public ActionResult TrainerMyStatistics(DateTime? startDate, DateTime? endDate, Guid? programId)
        {
            var currentUser = userProcesses.GetCurrentEnduser();

            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());

                    if (startDate == null && endDate == null && programId == null)
                    {
                        ViewBag.ProgramsOfTrainer = workoutProcesses.GetAllprogramsForTrainerById(currentUser.ID).Program;
                    }
                    else if (startDate != null && endDate != null && programId != null)
                    {
                        var programs = workoutProcesses.GetAllprogramsForTrainerById(currentUser.ID).Program;
                        ViewBag.ProgramsOfTrainer = programs.Where(_ => _.Id == programId && (_.CreateDate >= startDate && _.CreateDate <= endDate)).ToList();
                    }
                    else if (programId != null)
                    {
                        var programs = workoutProcesses.GetAllprogramsForTrainerById(currentUser.ID).Program;
                        ViewBag.ProgramsOfTrainer = programs.Where(_ => _.Id == programId).ToList();
                    }
                    else
                    {
                       var programs = workoutProcesses.GetAllprogramsForTrainerById(currentUser.ID).Program;
                        ViewBag.ProgramsOfTrainer = programs.Where(_ => _.CreateDate >= startDate && _.CreateDate <= endDate).ToList();
                    }
                    
                    ViewBag.RegisteredUsers = userProcesses.GetRegisteredUsersOfProgram(currentUser.ID);
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpGet]
        public ActionResult RegisteredUsers(Guid programId)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 1)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.RegisteredUsers = workoutProcesses.GetRegisteredUsersOfProgram(programId);
                    ViewBag.ProgramId = programId;
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("TrainerPanel", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpGet]
        public ActionResult TrainerRegisteredUsers(Guid programId)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.RegisteredUsers = workoutProcesses.GetRegisteredUsersOfProgram(programId);
                    ViewBag.ProgramId = programId;
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        
        [HttpGet]
        public ActionResult TrainerDetail(Guid trainerId)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 1)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Trainer = userProcesses.GetAllTrainers().FirstOrDefault(_ => _.ID == trainerId);
                    ViewBag.Certificates = userProcesses.GetCertificatesForUser(trainerId);

                    return View();
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("TrainerPanel", "Home");
                }
            }
            return View();
        }
        [HttpGet]
        public ActionResult AllPrograms(string message)
        {

            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 1)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Error = message;
                    var allPrograms = workoutProcesses.GetAllPrograms(true, true).Program;
                    var joinedPrograms = workoutProcesses.GetAllprogramsForEndUser().Program;

                    foreach (var program in joinedPrograms)
                    {
                        var removeItem = allPrograms.FirstOrDefault(_ => _.Id == program.Id);
                        allPrograms.Remove(removeItem);
                    }

                    ViewBag.Programs = allPrograms;
                    return View();
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("TrainerPanel", "Home");
                }
            }
            return View();
        }
        [HttpGet]
        public ActionResult TrainerPanel()
        {
            return RedirectToAction("MyWorkouts");
        }
        [HttpGet]
        public ActionResult AddWorkout(string error)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Error = error;
                    return View();
                }
                else
                    return RedirectToAction("Login", new { url = "AddWorkout" });
            }
            else
                return RedirectToAction("Login", new { url = "AddWorkout" });
        }
        [HttpGet]
        public ActionResult EditWorkout(Guid workoutID, string error)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    var workout = workoutProcesses.GetWorkout(new Business.Models.RequestModels.GetWorkoutRequest() { WorkoutID = workoutID });
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Workout = workout;
                    ViewBag.Error = error;
                    return View();
                }
                else
                    return RedirectToAction("Login", new { url = "MyWorkouts" });
            }
            else
                return RedirectToAction("Login", new { url = "MyWorkouts" });
        }

        public ActionResult DeleteWorkout(Guid workoutID)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    var workout = workoutProcesses.DeleteWorkout(workoutID);
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Workout = workout;
                    return RedirectToAction("MyWorkouts", "Home");
                }
                else
                    return RedirectToAction("Login", new { url = "MyWorkouts" });
            }
            else
                return RedirectToAction("Login", new { url = "MyWorkouts" });
        }
        [HttpGet]
        public ActionResult MyWorkouts()
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    var model = workoutProcesses.GetAllworkoutsForUser();
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    return View(model);
                }
                else
                    return RedirectToAction("Login", new { url = "MyWorkouts" });
            }
            else
                return RedirectToAction("Login", new { url = "MyWorkouts" });
        }
        [HttpGet]
        public ActionResult AddProgram()
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    var workouts = workoutProcesses.GetAllworkoutsForUser();

                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    return View(workouts);
                }
                else
                    return RedirectToAction("Login", new { url = "AddProgram" });
            }
            else
                return RedirectToAction("Login", new { url = "AddProgram" });
        }
        [HttpPost]
        public JsonResult RefreshMyWorkouts(string workoutCategory, string workoutClass)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();

            if (currentUser != null)
            {
                var workouts = workoutProcesses.GetAllworkoutsForUser();
                return
                    Json(
                        workouts.Workout.Where(_ => _.Category == workoutCategory && _.Class == workoutClass).ToList(),
                        JsonRequestBehavior.AllowGet);
            }
            else
                return Json(null, JsonRequestBehavior.AllowGet);

        }
        public ActionResult EditProgram(Guid programID)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    var program = workoutProcesses.GetProgram(new GetProgramRequest() { ProgramID = programID });
                    var workouts = workoutProcesses.GetAllworkoutsForUser();
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Program = program;
                    ViewBag.SelectedWorkouts = workoutProcesses.GetWorkoutsOfProgram(programID);
                    return View(workouts);
                }
                else
                    return RedirectToAction("Login", new { url = "MyPrograms" });
            }
            else
                return RedirectToAction("Login", new { url = "MyPrograms" });
        }
        public ActionResult DeleteProgram(Guid programID)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    var program = workoutProcesses.DeleteProgram(programID);
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Workout = program;

                    return RedirectToAction("MyPrograms", "Home");
                }
                else
                    return RedirectToAction("Login", new { url = "MyPrograms" });
            }
            else
                return RedirectToAction("Login", new { url = "MyPrograms" });
        }
        public ActionResult DeactiveProgram(Guid programID)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    var program = workoutProcesses.DeactivateProgram(programID);
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Workout = program;

                    return RedirectToAction("ActivateProgram", "Home");
                }
                else
                    return RedirectToAction("Login", new { url = "MyPrograms" });
            }
            else
                return RedirectToAction("Login", new { url = "MyPrograms" });
        }
        
        [HttpGet]
        public ActionResult MyPrograms()
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    var model = workoutProcesses.GetAllprogramsForUser();
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    return View(model);
                }
                else
                    return RedirectToAction("Login", new { url = "MyPrograms" });
            }
            else
                return RedirectToAction("Login", new { url = "MyPrograms" });
        }

        [HttpGet]
        public ActionResult MyProgramsUser(Guid programId, Guid userId)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null || programId != Guid.Empty)
            {
                if (currentUser.Role == 1)
                {
                    var program = workoutProcesses.GetProgram(new GetProgramRequest() { ProgramID = programId }).Program;
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Program = program;
                    return View();
                }
                else
                    return RedirectToAction("Login", new { url = "Index" });
            }
            else
                return RedirectToAction("Login", new { url = "Index" });
        }
        [HttpGet]
        public ActionResult MyAllProgramsUser()
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 1)
                {
                    var programs = workoutProcesses.GetAllprogramsForEndUser().Program;
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Programs = programs;
                    return View();
                }
                else
                    return RedirectToAction("Login", new { url = "Index" });
            }
            else
                return RedirectToAction("Login", new { url = "Index" });
        }

        [HttpGet]
        public ActionResult ActivateProgram(Guid? programId)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    if (programId == null)
                    {
                        var model = workoutProcesses.GetAllInActiveprogramsForUser();
                        ViewBag.User = currentUser;
                        ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                        return View(model);
                    }
                    else
                    {
                        if (workoutProcesses.ActivateProgram((Guid)programId))
                            return RedirectToAction("MyPrograms");
                        else
                            return RedirectToAction("ActivateProgram");
                    }
                }
                else
                    return RedirectToAction("Login", new { url = "ActivateProgram" });
            }
            else
                return RedirectToAction("Login", new { url = "ActivateProgram" });
        }
        [HttpGet]
        public ActionResult ProgramDetail(Guid programID)
        {
            var sessionHelper = new SessionProcesses();
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    var workouts = workoutProcesses.GetAllworkoutsForUser();
                    var program = workoutProcesses.GetProgram(new Business.Models.RequestModels.GetProgramRequest() { ProgramID = programID });
                    var comments =
                        workoutProcesses.GetComments(new GetCommentRequest()
                        {
                            CommentTarget = 2,
                            ProgWorkoutID = programID
                        });

                    List<Tuple<string, string, Guid>> CommentList = new List<Tuple<string, string, Guid>>();

                    foreach (var comment in comments.Comments)
                    {
                        CommentList.Add(new Tuple<string, string, Guid>(comment.Username, comment.Comment, comment.CommentID));
                    }

                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Workouts = workouts;
                    ViewBag.Program = program;
                    ViewBag.Comments = CommentList;
                    ViewBag.SelectedWorkouts = workoutProcesses.GetWorkoutsOfProgram(programID);

                    return View();
                }
                else
                    return RedirectToAction("Login", new { url = "MyPrograms" });
            }
            else
                return RedirectToAction("Login", new { url = "MyPrograms" });
        }
        [HttpGet]
        public ActionResult MyCertificates(string error)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    ViewBag.Certificates = userProcesses.GetCertificatesForUser(currentUser.ID);
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());
                    ViewBag.Error = error;
                    return View();
                }
                else
                    return RedirectToAction("Login", new { url = "MyCertificates" });
            }
            else
                return RedirectToAction("Login", new { url = "MyCertificates" });
        }
        [HttpGet]
        public ActionResult DeleteCertificate(string certificateNumber)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    userProcesses.DeleteCertification(certificateNumber);
                    return RedirectToAction("MyCertificates", "Home");
                }
                else
                    return RedirectToAction("Login", new { url = "MyCertificates" });
            }
            else
                return RedirectToAction("Login", new { url = "MyCertificates" });
        }
        [HttpGet]
        public ActionResult WeightLog(Guid? programId)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 1)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());

                    var weightLog = userProcesses.GetWeightLog(currentUser.ID, programId);

                    if (weightLog == null)
                    {
                        weightLog = new UserWeightChangeLog();
                    }

                    ViewBag.WeightLog = weightLog;
                    if (programId == null)
                    {
                        ViewBag.Programs = userProcesses.GetMyPrograms(currentUser.ID);
                    }
                    else
                    {
                        ViewBag.Programs = userProcesses.GetMyPrograms(currentUser.ID).Where(_ => _.ID == programId.Value).ToList();
                    }

                    return View();
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public ActionResult WeightLogOfUser(Guid programId, Guid userId)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 1)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());

                    var weightLog = userProcesses.GetWeightLog(userId, programId);

                    if (weightLog == null)
                    {
                        weightLog = new UserWeightChangeLog();
                    }

                    ViewBag.WeightLog = weightLog;

                    return View();
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult TrainerWeightLogOfUser(Guid programId, Guid userId)
        {
            var currentUser = userProcesses.GetCurrentEnduser();
            if (currentUser != null)
            {
                if (currentUser.Role == 2)
                {
                    ViewBag.User = currentUser;
                    ViewBagSet(currentUser.Username.ToUpper(), currentUser.ID.ToString());

                    var weightLog = userProcesses.GetWeightLog(userId, programId);

                    if (weightLog == null)
                    {
                        weightLog = new UserWeightChangeLog();
                    }

                    ViewBag.WeightLog = weightLog;

                    return View();
                }
                else if (currentUser.Role == 0)
                {
                    return RedirectToAction("RegisterRequests", "Admin");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region Top 10 Lists
        [HttpGet]
        public JsonResult GetTopTenProgramByRate()
        {
            try
            {
                return Json(workoutProcesses.GetAllPrograms(true, false), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public JsonResult GetTopTenProgramByViewCount()
        {
            try
            {
                return Json(workoutProcesses.GetAllPrograms(true, true), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public JsonResult GetTopTenTrainerByViewCount()
        {
            try
            {
                return Json(userProcesses.GetProfileListByViewCount(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public JsonResult GetMostWeightLostProgram()
        {
            try
            {
                return Json(workoutProcesses.GetAllProgramsByWeightLoss(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult GetCaptcha()
        {
            FileContentResult result;

            using (var memStream = new System.IO.MemoryStream())
            {
                var captchaHelper = new Captcha();
                var image = captchaHelper.GenerateCapthca();
                HttpContext.Response.ContentType = "image/jpg";
                image.Save(HttpContext.Response.OutputStream, ImageFormat.Jpeg);
                result = this.File(memStream.GetBuffer(), "image/jpeg");
            }

            return result;
        }


        #endregion

        #region Frontend Functions

        //[HttpGet]
        //public JsonResult GetTopTenPrograms()
        //{

        //}
        #endregion

        #region Common Functions
        private string ReadJson()
        {
            try
            {
                JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { DateFormatString = "dd-MM-yyyy" };
                Request.InputStream.Position = 0;

                using (var inputStream = new StreamReader(Request.InputStream))
                {
                    return inputStream.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        private void ViewBagSet(string username, string userId)
        {
            ViewBag.Username = username.ToUpper();
            ViewBag.UserId = userId;
        }
        #endregion
    }
}
