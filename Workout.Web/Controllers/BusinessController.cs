﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.IO;
using Workout.Business.Models.RequestModels;
using Workout.Business.Processes;
using Workout.Data.Processes;

namespace Workout.Web.Controllers
{
    public class BusinessController : Controller
    {
        #region Decleration
        private UserProcesses userProcesses = new UserProcesses();
        private string jsonString = String.Empty;

        #endregion

        #region Error Handling
        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            LogProcesses.ErrorLog(ex);
        }
        #endregion

        #region Registration
        [HttpPost]
        public JsonResult UserRegister()
        {
            try
            {
                var user = JsonConvert.DeserializeObject<UserRequests>(ReadJson());
                return Json(userProcesses.Register(user), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region User Update
        [HttpPost]
        public JsonResult UserUpdate()
        {
            try
            {
                var user = JsonConvert.DeserializeObject<UserUpdateRequests>(ReadJson());
                return Json(userProcesses.UpdateUser(user), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Login

        public JsonResult UserLogin()
        {
            try
            {
                var loginReq = JsonConvert.DeserializeObject<LoginRequest>(ReadJson());
                return Json(userProcesses.Login(loginReq), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult CheckLogin()
        {
            try
            {
                return Json(userProcesses.CheckLogin(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult Logout()
        {
            try
            {
                var result = userProcesses.Logout();
                if (result.Response.ResponseCode == 1)
                    return RedirectToAction("Login", "Home");
                else
                    return RedirectToAction("Error", "Home");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult CheckSecurityAnswer()
        {
            try
            {
                var securityReq = JsonConvert.DeserializeObject<SecurityAnswerRequest>(ReadJson());
                return Json(userProcesses.CheckSequrityAnswer(securityReq), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetUserWeight()
        {
            try
            {
                var getWeightReq = JsonConvert.DeserializeObject<GetWeightForUserRequest>(ReadJson());
                return Json(userProcesses.GetWeightForUser(getWeightReq));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetUserWeightForProgram()
        {
            try
            {
                var getWeightReq = JsonConvert.DeserializeObject<GetWeightForProgramRequest>(ReadJson());
                return Json(userProcesses.GetWeightForProgram(getWeightReq));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult SetUserWeight()
        {
            try
            {
                var setWeightReq = JsonConvert.DeserializeObject<SetWeightForUserRequest>(ReadJson());
                return Json(userProcesses.SetWeightForUser(setWeightReq));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Profile Functions
        public JsonResult GetProfileInformation()
        {
            try
            {
                return Json(userProcesses.GetProfile(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult UpdateProfile()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<UpdateProfileRequest>(ReadJson());
                return Json(userProcesses.UpdateProfile(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult HeightWeightUpdate()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<HeightWeightUpdateRequest>(ReadJson());
                return Json(userProcesses.UpdateHeightWeight(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult TrainerView()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<TrainerViewRequest>(ReadJson());
                return Json(userProcesses.TrainerViewUpdate(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult AddCertificate()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<AddCertificateRequest>(ReadJson());
                return Json(userProcesses.AddCertification(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Common Functions
        private string ReadJson()
        {
            try
            {
                JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { DateFormatString = "dd-MM-yyyy" };
                Request.InputStream.Position = 0;

                using (var inputStream = new StreamReader(Request.InputStream))
                {
                    return inputStream.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}