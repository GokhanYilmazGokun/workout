﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workout.Business.Models.RequestModels;
using Workout.Business.Processes;
using Workout.Data.Processes;

namespace Workout.Web.Controllers
{
    public class WorkoutController : Controller
    {
        #region Decleration
        private WorkoutProcesses workoutProcesses = new WorkoutProcesses();
        private string jsonString = String.Empty;

        #endregion

        #region Error Handling
        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            LogProcesses.ErrorLog(ex);
        }
        #endregion

        #region Workout Processes
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension == ".mp4")
                {
                    var fileID = Guid.NewGuid();
                    var path = Path.Combine(Server.MapPath("~/Assets/Videos"), fileID.ToString() + fileExtension);
                    file.SaveAs(path);
                    Session.Add("FileName", fileID.ToString() + fileExtension);
                }
                else
                {
                    return RedirectToAction("AddWorkout", "Home", new { error = "You can upload only mp4 files" });
                }
            }
            return RedirectToAction("AddWorkout", "Home");
        }
        [HttpPost]
        public ActionResult UploadFileForEdit(HttpPostedFileBase file, string workoutID)
        {
            if (file != null && file.ContentLength > 0)
            {
                var fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension == ".mp4")
                {
                    var fileID = Guid.NewGuid();
                    var path = Path.Combine(Server.MapPath("~/Assets/Videos"), fileID.ToString() + fileExtension);
                    file.SaveAs(path);
                    Session.Add("FileName", fileID.ToString() + fileExtension);
                }
                else
                {
                   return RedirectToAction("EditWorkout", "Home", new { workoutID = workoutID, error = "You can upload only mp4 files" });
                }
            }
            return RedirectToAction("EditWorkout", "Home", new { workoutID = workoutID });
        }

        [HttpPost]
        public ActionResult UploadCertificate(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension == ".pdf" || fileExtension == ".doc" || fileExtension == ".docx")
                {
                    var fileID = Guid.NewGuid();
                    var path = Path.Combine(Server.MapPath("~/Assets/Certificates"), fileID.ToString() + fileExtension);
                    file.SaveAs(path);
                    Session.Add("FileNameOfCertificate", fileID.ToString() + fileExtension);
                }
                else
                {
                    return RedirectToAction("MyCertificates", "Home", new { error = "You can upload only pdf/doc/docx files" });
                }
            }
            return RedirectToAction("MyCertificates", "Home");
        }

        //[HttpPost]
        //public ActionResult UploadCertificateForEdit(HttpPostedFileBase file, string certificateId)
        //{
        //    if (file != null && file.ContentLength > 0)
        //    {
        //        var fileExtension = Path.GetExtension(file.FileName);
        //        var fileID = Guid.NewGuid();
        //        var path = Path.Combine(Server.MapPath("~/Assets/Certificates"), fileID.ToString() + fileExtension);
        //        file.SaveAs(path);
        //        Session.Add("FileNameOfCertificate", fileID.ToString() + fileExtension);
        //    }
        //    return RedirectToAction("TrainerCertificates", "Admin");
        //}

        [HttpPost]
        public JsonResult CreateWorkout()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<WorkoutRequest>(ReadJson());
                return Json(workoutProcesses.CreateWorkout(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public JsonResult UpdateWorkout()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<UpdateWorkoutRequest>(ReadJson());
                return Json(workoutProcesses.UpdateWorkout(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public JsonResult CreateProgram()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<CreateProgramRequest>(ReadJson());
                return Json(workoutProcesses.CreateProgram(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public JsonResult UpdateProgram()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<UpdateProgramRequest>(ReadJson());
                return Json(workoutProcesses.UpdateProgram(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult GetWorkout()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<GetWorkoutRequest>(ReadJson());
                return Json(workoutProcesses.GetWorkout(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult AddWorkoutToProgram()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<AddToProgramRequest>(ReadJson());
                return Json(workoutProcesses.AddWorkoutToProgram(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult GetProgram()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<GetProgramRequest>(ReadJson());
                return Json(workoutProcesses.GetProgram(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult GetAllWorkouts()
        {
            try
            {
                return Json(workoutProcesses.GetAllWorkout(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public JsonResult GetAllPrograms()
        {
            try
            {
                return Json(workoutProcesses.GetAllPrograms(false, false), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult JoinToProgram()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<JoinToProgramRequest>(ReadJson());
                return Json(workoutProcesses.JoinToProgram(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public JsonResult JoinToProgram(Guid ProgramID)
        //{
        //    try
        //    {

        //        return Json(workoutProcesses.JoinToProgram(ProgramID), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public ActionResult JoinToProgram(Guid ProgramID)
        {
            try
            {
               var result = workoutProcesses.JoinToProgram(ProgramID);
                return RedirectToAction("AllPrograms", "Home", new {message = "Register Success"});
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Comment Processes
        [HttpPost]
        public JsonResult AddComment()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<AddCommentRequest>(ReadJson());
                return Json(workoutProcesses.AddComment(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public JsonResult GetComments()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<GetCommentRequest>(ReadJson());
                return Json(workoutProcesses.GetComments(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Rate Processes
        [HttpPost]
        public JsonResult AddRate()
        {
            try
            {
                var request = JsonConvert.DeserializeObject<AddRateRequest>(ReadJson());
                return Json(workoutProcesses.AddRate(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Common Functions
        private string ReadJson()
        {
            try
            {
                JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { DateFormatString = "dd-MM-yyyy" };
                Request.InputStream.Position = 0;

                using (var inputStream = new StreamReader(Request.InputStream))
                {
                    return inputStream.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}