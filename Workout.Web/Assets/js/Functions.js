﻿var BusinessController = "/Business";
var WorkoutController = "/Workout";
var AdminController = "/Admin";

$(document).ready(function () {


    $('.btn-login').click(function () {
        $('.login-form').validate(
        {
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: "required"
            },
            messages: {
                email: "Check the e-mail is valid or not",
                password: "Password is required"
            }
        }
            );
        if ($('.login-form').valid()) {
            var cryptedPassword = CryptoJS.SHA1($('.form-password').val(), { asBytes: true });
            var email = $('.form-email').val();
            DoLogin(email, cryptedPassword);
        }
    });

    $('.btn-register').click(function () {
        $('.register-form').validate(
        {
            rules: {
                name: {
                    required: true
                },
                surname: {
                    required: true
                },
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                birthdate: {
                    required: true
                },
                gender: {
                    required: true
                },
                question: {
                    required: true
                },
                answer: {
                    required: true
                }
            },
            messages: {
                name: "Name is required.",
                surname: "Surname is required",
                username: "Username is required",
                password: "Password is required",
                email: "Email is not valid",
                birthdate: "Birthdate is required",
                gender: "Gender is required",
                question: "Question is required",
                answer: "Answer is required"
            }
        });
        if ($('.register-form').valid()) {
            var cryptedPassword = CryptoJS.SHA1($('.form-password').val(), { asBytes: true });
            var username = $('.form-username').val();
            var name = $('.form-name').val();
            var surname = $('.form-username').val();
            var email = $('.form-email').val();
            var birthdate = $('.form-birthdate').val();
            var gender = $('.form-gender option:selected').val();
            var question = $('.form-question').val();
            var answer = $('.form-answer').val();
            DoRegister(cryptedPassword, username, name, surname, email, birthdate, gender, question, answer);
        };
    });
    $('.btn-update-profile').click(function () {
        $('.profile-form').validate(
      {
          rules: {
              name: {
                  required: true
              },
              surname: {
                  required: true
              },
              username: {
                  required: true
              },
              email: {
                  required: true,
                  email: true
              },
              birthdate: {
                  required: true
              },
              gender: {
                  required: true
              },
              question: {
                  required: true
              },
              answer: {
                  required: true
              }
          },
          messages: {
              name: "Name is required.",
              surname: "Surname is required",
              username: "Username is required",
              email: "Email is not valid",
              birthdate: "Birthdate is required",
              gender: "Gender is required",
              question: "Question is required",
              answer: "Answer is required"
          }
      });
        if ($('.profile-form').valid()) {
            var username = $('#form-profile-username').val();
            var name = $('#form-profile-name').val();
            var surname = $('#form-profile-surname').val();
            var email = $('#form-profile-email').val();
            var birthdate = $('.form-profile-birthdate').val();
            var gender = $('.form-profile-gender option:selected').val();
            var question = $('#form-profile-question').val();
            var answer = $('#form-profile-answer').val();
            var information = $('.information').val();
            DoUserUpdate(username, name, surname, email, birthdate, gender, question, answer, information);
        }
    });
    $('.btn-create-workout').click(function () {
        $('.create-workout-form').validate(
    {
        rules: {
            name: {
                required: true
            },
            title: {
                required: true
            },
            subtitle: {
                required: true
            },
            information: {
                required: true
            },
            category: {
                required: true
            },
            wclass: {
                required: true
            },
            video: {
                required: true
            }
        },
        messages: {
            name: "Name is required.",
            title: "Title is required",
            subtitle: "Subtitle is required",
            information: "Information is required",
            category: "Category is required",
            wclass: "Class is required",
            video: "Video is required please upload before send"
        }
    });
        if ($('.create-workout-form').valid()) {
            var name = $('#form-workout-name').val();
            var title = $('#form-workout-title').val();
            var information = $('#form-workout-information').val();
            var subtitle = $('#form-workout-subtitle').val();
            var workoutCategory = $('#form-workout-category option:selected').val();
            var workoutClass = $('#form-workout-class option:selected').val();
            var file = $('#form-workout-video').val();
            AddWorkout(name, title, information, subtitle, workoutCategory, workoutClass, file);
        }
    });
    $('.btn-update-workout').click(function () {
        $('.update-workout-form').validate(
  {
      rules: {
          name: {
              required: true
          },
          title: {
              required: true
          },
          subtitle: {
              required: true
          },
          information: {
              required: true
          },
          category: {
              required: true
          },
          wclass: {
              required: true
          },
          video: {
              required: true
          }
      },
      messages: {
          name: "Name is required.",
          title: "Title is required",
          subtitle: "Subtitle is required",
          information: "Information is required",
          category: "Category is required",
          wclass: "Class is required",
          video: "Video is required please upload before send"
      }
  });
        if ($('.update-workout-form').valid()) {
            var name = $('#form-workout-name').val();
            var title = $('#form-workout-title').val();
            var information = $('#form-workout-information').val();
            var subtitle = $('#form-workout-subtitle').val();
            var workoutCategory = $('#form-workout-category option:selected').val();
            var workoutClass = $('#form-workout-class option:selected').val();
            var file = $('#form-workout-video').val();
            var workoutID = $('#workout-id').val();
            UpdateWorkout(name, title, information, subtitle, workoutCategory, workoutClass, file, workoutID);
        }
    });
    $('.btn-create-program').click(function () {
        $('.create-program-form').validate(
{
    rules: {
        title: {
            required: true
        },
        subtitle: {
            required: true
        },
        information: {
            required: true
        }
    },
    messages: {
        name: "Name is required.",
        title: "Title is required",
        subtitle: "Subtitle is required",
        information: "Information is required"
    }
});
        if ($('.create-program-form').valid()) {
            var title = $('#title').val();
            var information = $('#information').val();
            var subtitle = $('#subtitle').val();
            var programCategory = $('#form-program-category option:selected').val();
            var programLevel = $('#form-program-level option:selected').val();
            var elems = $('#table-draggable2 > tbody  > tr').not(":first-child").not(":nth-child(2)");
            var workout = {};
            var selectedWorkouts = [];
            var workoutID;
            var period;
            var repeat;

            for (var i = 0; i < elems.length; i++) {
                var tdElements;
                $(elems[i]).each(function () { tdElements = $(this).find('td') });

                var j = 0;
                $(tdElements).each(function () {
                    if (j == 0) {
                        workoutID = $(this).data('id');
                        j++;
                    } else if (j == 3) {
                        period = $(this).find("input").val();
                        j++;
                    } else if (j == 4) {
                        repeat = $(this).find("input").val();
                        j++;
                        workout = { order: i, workout: workoutID, period: period, repeat: repeat };
                        selectedWorkouts.push(workout);
                    } else {
                        j++;
                    }
                });
            }
            AddProgram(title, information, subtitle, selectedWorkouts, programCategory, programLevel);
        }
    });
    $('.btn-update-program').click(function () {
        $('.update-program-form').validate(
{
    rules: {
        title: {
            required: true
        },
        subtitle: {
            required: true
        },
        information: {
            required: true
        }
    },
    messages: {
        name: "Name is required.",
        title: "Title is required",
        subtitle: "Subtitle is required",
        information: "Information is required"
    }
});
        if ($('.update-program-form').valid()) {

            var title = $('#title').val();
            var information = $('#information').val();
            var subtitle = $('#subtitle').val();
            var programCategory = $('#form-program-category option:selected').val();
            var programLevel = $('#form-program-level option:selected').val();
            var selectedWorkouts;
            var programID = $('.program-id').val();

            var elems = $('#table-draggable2 > tbody  > tr').not(":first-child").not(":nth-child(2)");
            var tdElements = [];

            for (var i = 0; i < elems.length; i++) {
                var tdElement = elems[i].getElementsByTagName('td');
                for (var j = 0; j < tdElement.length; j++) {
                    tdElements.push(tdElement[j]);
                }
            }

            var workout = {};
            var selectedWorkouts = [];
            var workoutID;
            var relationID;
            var period;
            var repeat;

            var j = 0;
            var i = 0;
            $(tdElements).each(function () {

                if (j == 0) {
                    workoutID = $(this).data('id');
                    relationID = $(this).data('relationid');
                    j++;
                } else if (j == 3) {
                    period = $(this).find("input").val();
                    j++;
                } else if (j == 4) {
                    repeat = $(this).find("input").val();
                    j++;
                    workout = { order: i, workout: workoutID, period: period, repeat: repeat, relationid: relationID };
                    selectedWorkouts.push(workout);
                    i++;
                    j = 0;
                } else {
                    j++;
                }
            });

            UpdateProgram(title, information, subtitle, selectedWorkouts, programID, programCategory, programLevel);
        }
    });
    $('.btn-add-comment').click(function () {
        $('.trainer-program-comment-form').validate(
{
    rules: {
        comment: {
            required: true
        }
    },
    messages: {
        comment: "Comment is required."
    }
});
        if ($('.trainer-program-comment-form').valid()) {
            var comment = $('.comment-text').val();
            var programID = $('.program-id').val();

            AddCommentToProgram(comment, programID);
        }
    });
    $('.btn-create-certificate').click(function () {
        $('.create-certificate-form').validate(
{
    rules: {
        name: {
            required: true
        },
        number: {
            required: true
        },
        certificateDate: {
            required: true
        },
        expireDate: {
            required: true
        }
    },
    messages: {
        name: "Certificate name is required.",
        number: "Cretificate number is required",
        certificateDate: "Certificate date is required",
        expireDate: "Certificate expire date is required"
    }
});
        if ($('.create-certificate-form').valid()) {
            var certificateName = $('#certificate-name').val();
            var certificateNumber = $('#certificate-number').val();
            var startDate = $('.form-certificate-date').val();
            var expireDate = $('.form-expire-date').val();
            var fileName = $('#form-trainer-certificate').val();
            CreateCertificate(certificateName, certificateNumber, startDate, expireDate, fileName);
        }
    });
    $('.comment-delete-btn').click(function () {
        if (deleteItem()) {
            var commentId = $(this).data('commentid');
            DeleteComment(commentId);
        }
    });
    $('.btn-update-weightlog').click(function () {
        var sessionId = getCookie('SID');
        var weight = $('#form-weightlog-weight').val();
        var fatRatio = $('#form-weightlog-fatratio').val();
        var muscleRatio = $('#form-weightlog-muscleratio').val();
        var programId = $('#selectProgram option:selected').val();

        UpdateWeightLog(sessionId, weight, fatRatio, muscleRatio, programId);

    });

    $('.certificate-search-btn').click(function () {
        var username = $('.certificate-search-input').val();
        window.location = '/Admin/TrainerCertificates?username=' + username;
    });
    $('.trainer-deactive-search-btn').click(function () {
        var username = $('.trainer-deactive-search-input').val();
        window.location = '/Admin/TrainerDeactivate?username=' + username;
    });
    $('.program-deactive-search-btn').click(function () {
        var program = $('.program-deactive-search-input').val();
        window.location = '/Admin/ProgramDeactivate?programTitle=' + program;
    });
    $('.user-deactive-search-btn').click(function () {
        var username = $('.user-deactive-search-input').val();
        window.location = '/Admin/UserDeactivate?username=' + username;
    });



});


//function GetDashboardData() {
//    $.ajax({
//        type: "POST",
//        url: ControllerName + '/GetDashboardInformation',
//        success: function (data) {
//            if (data.Response.StatusCode == 1) {
//                $('.event-count').text(data.EventCount);
//                $('.event-place-count').text(data.EventPlaceCount);
//                $('.scheme-count').text(data.SchemeCount);
//                $('.user-count').text(data.UserCount);
//            }
//        }
//    });
//}

function DoLogin(email, password) {

    $.ajax({
        type: "POST",
        url: BusinessController + '/UserLogin',
        contentType: "application/json; charset=utf-8",
        data: "{ Email:\"" + email + "\", Password: \"" + password + "\" }",
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                if (data.Role == 1) {
                    window.location.href = '/Home/Index/';
                }
                else {
                    window.location.href = '/Home/TrainerPanel/';
                }
            }
            else if (data.Response.ResponseCode == -4) {
                alert(data.Response.ResponseMessage);
            }
            else {
                alert("Login Failed");
            }
        }
    });
}

function DoRegister(password, username, name, surname, email, birthdate, gender, question, answer) {
    $.ajax({
        type: "POST",
        url: BusinessController + '/UserRegister',
        contentType: "application/json; charset=utf-8",
        data: "{ Username:\"" + username + "\", Name: \"" + name + "\", Surname: \"" + surname + "\", Question: \"" + question + "\", Answer: \"" + answer + "\", Password: \"" + password + "\", Email: \"" + email + "\", Birthdate: \"" + birthdate + "\", Gender: " + gender + " }",
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                window.location.href = '/Home/Login/';
            }
            else {
                alert(data.Response.ResponseMessage);
            }
        }
    });
}

function DoUserUpdate(username, name, surname, email, birthdate, gender, question, answer, information) {
    $.ajax({
        type: "POST",
        url: BusinessController + '/UserUpdate',
        contentType: "application/json; charset=utf-8",
        data: "{ Username:\"" + username + "\", Information:\"" + information + "\", Name: \"" + name + "\", Surname: \"" + surname + "\", Question: \"" + question + "\", Answer: \"" + answer + "\", Email: \"" + email + "\", Birthdate: \"" + birthdate + "\", Gender: " + gender + " }",
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                window.location.href = '/Home/Profile/';
            }
        }
    });
}

function AddWorkout(name, title, information, subtitle, workoutCategory, workoutClass, file) {
    $.ajax({
        type: "POST",
        url: WorkoutController + '/CreateWorkout',
        contentType: "application/json; charset=utf-8",
        data: "{ Name:\"" + name + "\", Information: \"" + information + "\", Title: \"" + title + "\", Subtitle: \"" + subtitle + "\", Category: " + workoutCategory + ", Class: " + workoutClass + ", Video: \"" + file + "\" }",
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                alert("Register Success");
            }
            else {
                alert("Register Failed");
            }
        }
    });
}

function UpdateWorkout(name, title, information, subtitle, workoutCategory, workoutClass, file, workoutID) {
    $.ajax({
        type: "POST",
        url: WorkoutController + '/UpdateWorkout',
        contentType: "application/json; charset=utf-8",
        data: "{ WorkoutID: \"" + workoutID + "\", Name:\"" + name + "\", Information: \"" + information + "\", Title: \"" + title + "\", Subtitle: \"" + subtitle + "\", Category: " + workoutCategory + ", Class: " + workoutClass + ", Video: \"" + file + "\" }",
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                alert("Register Success");
            }
            else {
                alert("Register Failed");
            }
        }
    });
}

function AddProgram(title, information, subtitle, selectedWorkouts, programCategory, programLevel) {
    $.ajax({
        type: "POST",
        url: WorkoutController + '/CreateProgram',
        contentType: "application/json; charset=utf-8",
        data: "{ Information: \"" + information + "\", Title: \"" + title + "\", Subtitle: \"" + subtitle + "\",  SelectedWorkouts: " + JSON.stringify(selectedWorkouts) + ", ProgramLevel: \"" + programLevel + "\", ProgramCategory: \"" + programCategory + "\" }",
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                alert("Register Success");
                window.location.href = '/Home/MyPrograms/';
            }
            else {
                alert("Register Failed");
            }
        }
    });
}

function UpdateProgram(title, information, subtitle, selectedWorkouts, programID, programCategory, programLevel) {
    $.ajax({
        type: "POST",
        url: WorkoutController + '/UpdateProgram',
        contentType: "application/json; charset=utf-8",
        data: "{ProgramID:\"" + programID + "\", Information: \"" + information + "\", Title: \"" + title + "\", Subtitle: \"" + subtitle + "\",  SelectedWorkouts: " + JSON.stringify(selectedWorkouts) + ", ProgramCategory: \"" + programCategory + "\", ProgramLevel: \"" + programLevel + "\" }",
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                window.location.href = '/Home/MyPrograms/';
            }
            else if (data.Response.ResponseCode == -5) {
                alert("Program cannot edit because users joined this program");
            }
            else {
                alert("Register Failed");
            }
        }
    });
}

function AddCommentToProgram(comment, programID) {
    $.ajax({
        type: "POST",
        url: WorkoutController + '/AddComment',
        contentType: "application/json; charset=utf-8",
        data: "{ProgWorkoutID:\"" + programID + "\", Message: \"" + comment + "\", CommentTarget: 2 }",
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                location.reload();
            }
            else {
                alert("Comment insert Failed");
            }
        }
    });
}

function DeleteComment(commentId) {
    $.ajax({
        type: "POST",
        url: AdminController + '/DeleteComment',
        contentType: "application/json; charset=utf-8",
        data: "{CommentID:\"" + commentId + "\"}",
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                location.reload();
            }
            else {
                alert("Register Failed");
            }
        }
    });
}

function CreateCertificate(certificateName, certificateNumber, startDate, expireDate, filename) {
    $.ajax({
        type: "POST",
        url: BusinessController + '/AddCertificate',
        contentType: "application/json; charset=utf-8",
        data: "{CertificateName:\"" + certificateName + "\",  Filename:\"" + filename + "\", CertificateName:\"" + certificateName + "\", CertififcationNumber: \"" + certificateNumber + "\", StartDate: \"" + startDate + "\",  ExpireDate: \"" + expireDate + "\"}",
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                location.reload();
            }
            else {
                alert("Register Failed");
            }
        }
    });
}

function deleteItem() {
    if (confirm("Are you sure ? To do this action")) {
        return true;
    }
    return false;
}

function GetFilteredWorkouts() {
    var workoutCategory = $('#form-workout-category option:selected').text();
    var workoutClass = $('#form-workout-class option:selected').text();

    $.ajax({
        type: "POST",
        url: '/Home/RefreshMyWorkouts',
        contentType: "application/json; charset=utf-8",
        data: "{workoutCategory:\"" + workoutCategory + "\", workoutClass: \"" + workoutClass + "\"}",
        success: function (data) {
            $('.my-workouts-tr').next().nextAll().remove();
            $.each(data, function (index, val) {
                $('.my-workouts-tr').next().after("<tr><td data-id='" + val.ID + "'>" + val.Title + "</td><td>" + val.Category + "</td><td>" + val.Class + "</td><td><input type='number' class='workout-period' id='period' value='0' min='0'  style='width: 50px;'/></td><td><input type='number' class='workout-repeat' id='number' value='0' min='0' style='width: 50px;'/></td></tr>");
            });
        }
    });


}

function UpdateWeightLog(sessionId, weight, fatRatio, muscleRatio, programId) {
    $.ajax({
        type: "POST",
        url: BusinessController + '/SetUserWeight',
        contentType: "application/json; charset=utf-8",
        data: "{SessionId:\"" + sessionId + "\",Weight:\"" + weight + "\",FatRatio:\"" + fatRatio + "\",MuscleRatio:\"" + muscleRatio + "\",ProgramId:\"" + programId + "\"}",
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                location.reload();
            }
            else {
                alert("Failed");
            }
        }
    });
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

function GetWeightChangeLog() {
    var sessionId = getCookie('SID');
    var programId = location.search.split('programId=')[1];

    if (typeof programId === "undefined")
        programId = "0";

    var result;
    $.ajax({
        type: "POST",
        url: BusinessController + '/GetUserWeight',
        contentType: "application/json; charset=utf-8",
        data: "{SessionId:\"" + sessionId + "\",ProgramId:\"" + programId + "\"}",
        async:false,
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                result = data;
            }
            else {
                alert("Failed");
            }
        }
    });
    return result;
}
function GetWeightChangeLogForProgram() {
    var parseQueryString = function () {

        var str = window.location.search;
        var objURL = {};

        str.replace(
            new RegExp("([^?=&]+)(=([^&]*))?", "g"),
            function ($0, $1, $2, $3) {
                objURL[$1] = $3;
            }
        );
        return objURL;
    };
    var params = parseQueryString();

    var programId = params["programId"];
    var userId = params["userId"];

    if (typeof programId === "undefined")
        programId = "0";

    var result;
    $.ajax({
        type: "POST",
        url: BusinessController + '/GetUserWeightForProgram',
        contentType: "application/json; charset=utf-8",
        data: "{ProgramId:\"" + programId + "\",UserId:\"" + userId + "\"}",
        async: false,
        success: function (data) {

            if (data.Response.ResponseCode == 1) {
                result = data;
            }
            else {
                alert("Failed");
            }
        }
    });
    return result;
}

function trainerSearch() {
    var username = $('.search-query').val();

    window.location.href = "/Home/Trainers?username=" + username;
}
function trainerSearchAdmin() {
    var username = $('.search-query').val();

    window.location.href = "/Admin/Trainers?username=" + username;
}

function TrainerStatisticsEndUser() {
    var parseQueryString = function () {

        var str = window.location.search;
        var objURL = {};

        str.replace(
            new RegExp("([^?=&]+)(=([^&]*))?", "g"),
            function ($0, $1, $2, $3) {
                objURL[$1] = $3;
            }
        );
        return objURL;
    };
    var params = parseQueryString();

    var trainerId = params["trainerId"];
    var startDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    var selectedProgram = $('#selectProgram option:selected').val();
    var href = window.location.href.split(/[?#]/)[0];

    if (startDate != "" && endDate != "" && selectedProgram == "0") {
       
        window.location.href = href+"?trainerId=" + trainerId + "&startDate=" + startDate + "&endDate=" + endDate;
    }
    if ( startDate != "" &&  endDate != "" && selectedProgram != "0") {
        window.location.href = href + "?trainerId=" + trainerId + "&startDate=" + startDate + "&endDate=" + endDate + "&programId=" + selectedProgram;
    }
    if ( startDate == "" &&  endDate == "" && selectedProgram != "0") {
        window.location.href = href + "?trainerId=" + trainerId + "&programId=" + selectedProgram;
    }
    if (startDate == "" && endDate == "" && selectedProgram == "0") {

        window.location.href = href + "?trainerId=" + trainerId + "";
    }
}

function TrainerMyStatistics() {
    var startDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    var selectedProgram = $('#selectProgram option:selected').val();

    if (startDate != "" &&  endDate != "" && selectedProgram == "0") {
        window.location.search = "startDate=" + startDate + "&endDate=" + endDate;
    }
    if (startDate != "" &&  endDate != "" && selectedProgram != "0") {
        window.location.search = "startDate=" + startDate + "&endDate=" + endDate + "&programId=" + selectedProgram;
    }
    if (startDate == "" &&  endDate == "" && selectedProgram != "0") {
        window.location.search = "programId=" + selectedProgram;
    }
    if ( startDate == "" &&  endDate == "" && selectedProgram == "0") {
        window.location.search = "";
    }

}