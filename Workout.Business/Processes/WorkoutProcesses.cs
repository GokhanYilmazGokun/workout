﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Workout.Business.Models.Enums;
using Workout.Business.Models.RequestModels;
using Workout.Business.Models.ResponseModels;
using Workout.Data;
using Workout.Data.Models;
using Workout.Data.Processes;

namespace Workout.Business.Processes
{
    public class WorkoutProcesses
    {
        #region Definations
        private DataProcesses dataHelper = new DataProcesses();
        private CRUDProcesses crudHelper = new CRUDProcesses();
        private SessionProcesses sessionHelper = new SessionProcesses();

        #endregion

        #region Workout and Program Processes
        public CreateWorkoutResponse CreateDefaultWorkout(WorkoutRequest request, Guid userId)
        {
            try
            {
                var workoutID = Guid.NewGuid();

                var workout = new Workouts() { ID = workoutID, Title = request.Title, Subtitle = request.Subtitle, Name = request.Name, Creator = userId, Video = request.Video, Information = request.Information, IsActive = 1, Rate = 0.0, Category = request.Category, WClass = request.Class };
                var rate = new Rates() { ID = workoutID, Five = 0, Four = 0, Three = 0, Two = 0, One = 0 };

                if (crudHelper.Add<Workouts>(workout) && crudHelper.Add<Rates>(rate))
                    return new CreateWorkoutResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };

                else
                    return new CreateWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "Workout create failed" } };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CreateWorkoutResponse CreateWorkout(WorkoutRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user.Role == (int)UserRole.Trainer)
                        {
                            var workoutID = Guid.NewGuid();

                            var workout = new Workouts() { ID = workoutID, Title = request.Title, Subtitle = request.Subtitle, Name = request.Name, Creator = user.ID, Video = request.Video, Information = request.Information, IsActive = 1, Rate = 0.0, Category = request.Category, WClass = request.Class };
                            var rate = new Rates() { ID = workoutID, Five = 0, Four = 0, Three = 0, Two = 0, One = 0 };

                            if (crudHelper.Add<Workouts>(workout) && crudHelper.Add<Rates>(rate))
                                return new CreateWorkoutResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };

                            else
                                return new CreateWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "Workout create failed" } };
                        }
                        else
                            return new CreateWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Permisson is denied for this process" } };
                    }
                    else
                        return new CreateWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new CreateWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UpdateWorkoutResponse UpdateWorkout(UpdateWorkoutRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user.Role == (int)UserRole.Trainer)
                        {
                            var workout = dataHelper.GetWorkoutWithId(request.WorkoutID);

                            workout.Name = request.Name;
                            workout.Title = request.Title;
                            workout.Subtitle = request.Subtitle;
                            workout.Information = request.Information;
                            workout.Category = request.Category;
                            workout.WClass = request.Class;
                            workout.Video = request.Video;

                            crudHelper.UpdateWorkout(workout);

                            return new UpdateWorkoutResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                        }
                        else
                            return new UpdateWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Permisson is denied for this process" } };
                    }
                    else
                        return new UpdateWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new UpdateWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<WorkoutDetailsResponse> GetWorkoutsOfProgram(Guid programID)
        {
            try
            {
                return dataHelper.GetWorkoutsOfProgram(programID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public WorkoutResponse GetWorkout(GetWorkoutRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var workout = dataHelper.GetWorkoutWithId(request.WorkoutID);
                            var comments = GetComments(new GetCommentRequest() { CommentTarget = (int)CommentTarget.Workout, ProgWorkoutID = workout.ID });
                            var rate = dataHelper.GetRateByID(request.WorkoutID);
                            double calculatedRate = CalculateRate(rate);

                            return new WorkoutResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, Workout = new WorkoutDetail() { Name = workout.Name, Title = workout.Title, Subtitle = workout.Subtitle, Information = workout.Information, Video = workout.Video, Rate = calculatedRate, Creator = user.Username, Comments = comments.Comments, Category = Enum.GetName(typeof(WorkoutCategory), workout.Category), Class = Enum.GetName(typeof(WorkoutClass), workout.WClass), IsActive = workout.IsActive, ID = workout.ID } };

                        }
                        else
                            return new WorkoutResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Workout = null };
                    }
                    else
                        return new WorkoutResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Workout = null };
                }
                else
                    return new WorkoutResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Workout = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public WorkoutResponse DeleteWorkout(Guid workoutID)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var workout = dataHelper.GetWorkoutWithIdForUser(user.ID, workoutID);
                            crudHelper.DeleteWorkout(workout);

                            return new WorkoutResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, Workout = null };
                        }
                        else
                            return new WorkoutResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Workout = null };
                    }
                    else
                        return new WorkoutResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Workout = null };
                }
                else
                    return new WorkoutResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Workout = null };
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public AllWorkoutResponse GetAllWorkout()
        {
            try
            {
                var response = new AllWorkoutResponse();
                response.Workout = new List<WorkoutDetail>();
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var workouts = dataHelper.GetAllWorkouts();

                            foreach (var workout in workouts)
                            {
                                var comments = GetComments(new GetCommentRequest() { CommentTarget = (int)CommentTarget.Workout, ProgWorkoutID = workout.ID });
                                var rate = dataHelper.GetRateByID(workout.ID);
                                double calculatedRate = CalculateRate(rate);

                                response.Workout.Add(new WorkoutDetail() { Name = workout.Name, Title = workout.Title, Subtitle = workout.Subtitle, Information = workout.Information, Video = workout.Video, Rate = calculatedRate, Creator = user.Username, Comments = comments.Comments, IsActive = workout.IsActive, Category = Enum.GetName(typeof(WorkoutCategory), workout.Category), Class = Enum.GetName(typeof(WorkoutClass), workout.WClass), ID = workout.ID });
                            }

                            response.Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" };
                            return response;
                        }
                        else
                            return new AllWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Workout = null };
                    }
                    else
                        return new AllWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Workout = null };
                }
                else
                    return new AllWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Workout = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public AllWorkoutResponse GetAllworkoutsForUser()
        {
            try
            {
                var response = new AllWorkoutResponse();
                response.Workout = new List<WorkoutDetail>();
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var workouts = dataHelper.GetWorkoutListForUser(user.ID);

                            foreach (var workout in workouts)
                            {
                                var comments = GetComments(new GetCommentRequest() { CommentTarget = (int)CommentTarget.Workout, ProgWorkoutID = workout.ID });
                                var rate = dataHelper.GetRateByID(workout.ID);
                                double calculatedRate = CalculateRate(rate);

                                response.Workout.Add(new WorkoutDetail() { Name = workout.Name, Title = workout.Title, Subtitle = workout.Subtitle, Information = workout.Information, Video = workout.Video, Rate = calculatedRate, Creator = user.Username, Comments = (comments.Comments.Any()) ? comments.Comments : null, IsActive = workout.IsActive, Category = Enum.GetName(typeof(WorkoutCategory), workout.Category), Class = Enum.GetName(typeof(WorkoutClass), workout.WClass), ID = workout.ID });
                            }

                            response.Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" };
                            return response;
                        }
                        else
                            return new AllWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Workout = null };
                    }
                    else
                        return new AllWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Workout = null };
                }
                else
                    return new AllWorkoutResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Workout = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public AllProgramResponse GetAllprogramsForUser()
        {
            try
            {
                var response = new AllProgramResponse();
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    response.Program = new List<ProgramDetail>();
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var programs = dataHelper.GetProgramListForUser(user.ID);

                            foreach (var program in programs)
                            {
                                var comments = GetComments(new GetCommentRequest() { CommentTarget = (int)CommentTarget.Workout, ProgWorkoutID = program.ID });
                                var rate = dataHelper.GetRateByID(program.ID);
                                double calculatedRate = CalculateRate(rate);
                                response.Program.Add(new ProgramDetail()
                                {
                                    Title = program.Title,
                                    Subtitle = program.Subtitle,
                                    Information = program.Information,
                                    Creator = user.Username,
                                    ViewCount = 0,
                                    RegisteredUsers = GetRegisteredUsernames(program.ID),
                                    Id = program.ID,
                                    AvarageWeightLoss = 0.0,
                                    Rate = CalculateRate(dataHelper.GetRateByID(program.ID)),
                                    IsActive = program.IsActive,
                                    Category = Enum.GetName(typeof(ProgramCategory), program.ProgramCategory),
                                    CommentCount = comments.Comments.Count
                                });
                            }

                            response.Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" };
                            return response;
                        }
                        else
                            return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                    }
                    else
                        return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                }
                else
                    return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Program = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AllProgramResponse GetAllprogramsForTrainerById(Guid trainerId)
        {
            try
            {
                var response = new AllProgramResponse();
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    response.Program = new List<ProgramDetail>();
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var programs = dataHelper.GetProgramListForUser(trainerId);
                            var userPrograms = dataHelper.GetProgramListForEndUser(user.ID);

                            foreach (var program in programs)
                            {
                                foreach (var p in userPrograms)
                                {
                                    if (p.ID == program.ID)
                                    {
                                        var comments = GetComments(new GetCommentRequest() { CommentTarget = (int)CommentTarget.Workout, ProgWorkoutID = program.ID });
                                        var rate = dataHelper.GetRateByID(program.ID);
                                        double calculatedRate = CalculateRate(rate);
                                        response.Program.Add(new ProgramDetail()
                                        {
                                            Title = program.Title,
                                            Subtitle = program.Subtitle,
                                            Information = program.Information,
                                            Creator = program.Creator.ToString(),
                                            ViewCount = program.ViewCount,
                                            RegisteredUsers = GetRegisteredUsernames(program.ID),
                                            Id = program.ID,
                                            AvarageWeightLoss = 0.0,
                                            Rate = calculatedRate,
                                            IsActive = program.IsActive,
                                            Category = Enum.GetName(typeof(ProgramCategory), program.ProgramCategory),
                                            CommentCount = comments.Comments.Count,
                                            CreateDate = program.CreateDate.Value
                                        });
                                    }
                                }

                            }

                            response.Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" };
                            return response;
                        }
                        else
                            return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                    }
                    else
                        return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                }
                else
                    return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Program = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public AllProgramResponse GetAllprogramsForTrainerByIdAdmin(Guid trainerId)
        {
            try
            {
                var response = new AllProgramResponse();
                var cookie = HttpContext.Current.Request.Cookies["SIDA"];

                if (cookie != null)
                {
                    Guid ID;
                    response.Program = new List<ProgramDetail>();
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = sessionHelper.GetCurrentUser(UserRole.Admin);

                        if (user != null)
                        {
                            var programs = dataHelper.GetProgramListForUser(trainerId);

                            foreach (var program in programs)
                            {
                                var comments = GetCommentsByAdmin(new GetCommentRequest() { CommentTarget = (int)CommentTarget.Workout, ProgWorkoutID = program.ID });
                                var rate = dataHelper.GetRateByID(program.ID);
                                double calculatedRate = CalculateRate(rate);
                                response.Program.Add(new ProgramDetail()
                                {
                                    Title = program.Title,
                                    Subtitle = program.Subtitle,
                                    Information = program.Information,
                                    Creator = program.Creator.ToString(),
                                    ViewCount = program.ViewCount,
                                    RegisteredUsers = GetRegisteredUsernames(program.ID),
                                    Id = program.ID,
                                    AvarageWeightLoss = 0.0,
                                    Rate = calculatedRate,
                                    IsActive = program.IsActive,
                                    Category = Enum.GetName(typeof(ProgramCategory), program.ProgramCategory),
                                    CommentCount = comments.Comments.Count,
                                    CreateDate = program.CreateDate.Value
                                });
                            }

                            response.Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" };
                            return response;
                        }
                        else
                            return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                    }
                    else
                        return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                }
                else
                    return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Program = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Users> GetRegisteredUsersOfProgram(Guid programId)
        {
            try
            {
                var response = new AllProgramResponse();
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    response.Program = new List<ProgramDetail>();
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            return dataHelper.GetRegisteredUsersOfProgram(programId);
                        }
                        else
                            return new List<Users>();
                    }
                    else
                        return new List<Users>();
                }
                else
                    return new List<Users>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Users> GetRegisteredUsersOfProgramByAdmin(Guid programId)
        {
            try
            {
                var response = new AllProgramResponse();
                var cookie = HttpContext.Current.Request.Cookies["SIDA"];

                if (cookie != null)
                {
                    Guid ID;
                    response.Program = new List<ProgramDetail>();
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = sessionHelper.GetCurrentUser(UserRole.Admin);

                        if (user != null)
                        {
                            return dataHelper.GetRegisteredUsersOfProgram(programId);
                        }
                        else
                            return new List<Users>();
                    }
                    else
                        return new List<Users>();
                }
                else
                    return new List<Users>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public AllProgramResponse GetAllprogramsForEndUser()
        {
            try
            {
                var response = new AllProgramResponse();
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    response.Program = new List<ProgramDetail>();
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var programs = dataHelper.GetProgramListForEndUser(user.ID);

                            foreach (var program in programs)
                            {
                                var comments = GetComments(new GetCommentRequest() { CommentTarget = (int)CommentTarget.Workout, ProgWorkoutID = program.ID });
                                var rate = dataHelper.GetRateByID(program.ID);
                                double calculatedRate = CalculateRate(rate);
                                response.Program.Add(new ProgramDetail() { Title = program.Title, Subtitle = program.Subtitle, Information = program.Information, Creator = user.Username, ViewCount = 0, RegisteredUsers = GetRegisteredUsernames(program.ID), Id = program.ID, AvarageWeightLoss = 0.0, Rate = CalculateRate(dataHelper.GetRateByID(program.ID)), IsActive = program.IsActive, Category = Enum.GetName(typeof(ProgramCategory), program.ProgramCategory) });
                            }

                            response.Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" };
                            return response;
                        }
                        else
                            return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                    }
                    else
                        return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                }
                else
                    return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Program = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AllProgramResponse GetAllInActiveprogramsForUser()
        {
            try
            {
                var response = new AllProgramResponse();
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    response.Program = new List<ProgramDetail>();
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var programs = dataHelper.GetInActiveProgramListForUser(user.ID);

                            foreach (var program in programs)
                            {
                                var comments = GetComments(new GetCommentRequest() { CommentTarget = (int)CommentTarget.Workout, ProgWorkoutID = program.ID });
                                var rate = dataHelper.GetRateByID(program.ID);
                                double calculatedRate = CalculateRate(rate);
                                response.Program.Add(new ProgramDetail() { Title = program.Title, Subtitle = program.Subtitle, Information = program.Information, Creator = user.Username, ViewCount = 0, RegisteredUsers = GetRegisteredUsernames(program.ID), Id = program.ID, AvarageWeightLoss = 0.0, Rate = CalculateRate(dataHelper.GetRateByID(program.ID)), IsActive = program.IsActive });
                            }

                            response.Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" };
                            return response;
                        }
                        else
                            return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                    }
                    else
                        return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                }
                else
                    return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Program = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CreateProgramResponse CreateProgram(CreateProgramRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user.Role == (int)UserRole.Trainer || user.Role == (int)UserRole.Admin)
                        {
                            var programID = Guid.NewGuid();
                            var program = new Programs()
                            {
                                ID = programID,
                                Creator = user.ID,
                                Title = request.Title,
                                Subtitle = request.Subtitle,
                                Information = request.Information,
                                IsActive = 0,
                                Rate = 0.0,
                                CreateDate = DateTime.Now,
                                ProgramCategory = request.ProgramCategory,
                                ProgramLevel = request.ProgramLevel,
                                IsEditable = 1,
                                IsDeleted = 0
                            };
                            var rate = new Rates() { ID = programID, Five = 0, Four = 0, Three = 0, Two = 0, One = 0 };
                            crudHelper.Add<Programs>(program);

                            foreach (var item in request.SelectedWorkouts)
                            {
                                var relation = new PWRelation();
                                relation.ID = Guid.NewGuid();
                                relation.ProgramID = programID;
                                relation.WorkoutID = Guid.Parse(item.workout);
                                relation.Queue = item.order;
                                relation.IsActive = 1;
                                relation.Period = item.period;
                                relation.Repeat = item.repeat;
                                crudHelper.Add<PWRelation>(relation);
                            }

                            if (crudHelper.Add<Rates>(rate))
                            {
                                return new CreateProgramResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                            }

                            else
                                return new CreateProgramResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "Creation failed" } };
                        }
                        else
                            return new CreateProgramResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Permisson is denied for this process" } };
                    }
                    else
                        return new CreateProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new CreateProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public UpdateProgramResponse UpdateProgram(UpdateProgramRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user.Role == (int)UserRole.Trainer || user.Role == (int)UserRole.Admin)
                        {
                            var program = dataHelper.GetProgramWithId(request.ProgramID);
                            if (program.IsEditable == 1)
                            {
                                program.Title = request.Title;
                                program.Subtitle = request.Subtitle;
                                program.Information = request.Information;
                                program.CreateDate = DateTime.Now;
                                program.ProgramCategory = request.ProgramCategory;
                                program.ProgramLevel = request.ProgramLevel;
                                crudHelper.UpdateProgram(program);

                                foreach (var item in request.SelectedWorkouts)
                                {
                                    var relation = !string.IsNullOrEmpty(item.relationid) ? dataHelper.GetPWRelation(Guid.Parse(item.workout), program.ID, Guid.Parse(item.relationid)) : null;

                                    if (relation == null)
                                    {
                                        var workoutTemp = dataHelper.GetWorkoutWithId(Guid.Parse(item.workout));
                                        var r = new PWRelation();
                                        r.ID = Guid.NewGuid();
                                        r.Queue = item.order;
                                        r.Period = item.period;
                                        r.Repeat = item.repeat;
                                        r.IsActive = 1;
                                        r.ProgramID = program.ID;
                                        r.WorkoutID = Guid.Parse(item.workout);
                                        crudHelper.AddRelation(program, workoutTemp, r);
                                    }
                                    else
                                    {
                                        relation.Queue = item.order;
                                        relation.Period = item.period;
                                        relation.Repeat = item.repeat;
                                        crudHelper.UpdatePWRelation(relation);
                                    }
                                }

                                return new UpdateProgramResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                            }
                            else
                            {
                                return new UpdateProgramResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "Failed : Program cannot editable" } };
                            }
                        }
                        else
                            return new UpdateProgramResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Permisson is denied for this process" } };
                    }
                    else
                        return new UpdateProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new UpdateProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProgramResponse GetProgram(GetProgramRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var program = dataHelper.GetProgramWithId(request.ProgramID);
                            var comments = GetComments(new GetCommentRequest() { CommentTarget = (int)CommentTarget.Program, ProgWorkoutID = program.ID });
                            var registeredUsers = GetRegisteredUsernames(program.ID);
                            var rate = dataHelper.GetRateByID(request.ProgramID);
                            double calculatedRate = CalculateRate(rate);

                            return new ProgramResponse()
                            {
                                Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" },
                                Program = new ProgramDetail()
                                {
                                    Creator = user.Username,
                                    Information = program.Information,
                                    Rate = calculatedRate,
                                    RegisteredUsers = registeredUsers,
                                    Subtitle = program.Subtitle,
                                    Title = program.Title,
                                    Id = program.ID,
                                    ViewCount = program.ViewCount,
                                    Category = Enum.GetName(typeof(ProgramCategory), program.ProgramCategory),
                                    Level = Enum.GetName(typeof(ProgramLevels), program.ProgramLevel),
                                    ProgramLevel = program.ProgramLevel.Value,
                                    ProgramCategory = program.ProgramCategory.Value
                                }
                            };
                        }
                        else
                            return new ProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                    }
                    else
                        return new ProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                }
                else
                    return new ProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Program = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Programs> GetProgramsWithTitle(string title, int userRole)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (userRole == 0) //ADmin
                {
                    cookie = HttpContext.Current.Request.Cookies["SIDA"];
                }

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        if (userRole == 0)
                        {
                            return dataHelper.GetProgramsWithTitle(title);
                        }
                        else
                        {
                            var user = dataHelper.GetUser(ID);

                            if (user != null)
                            {
                                return dataHelper.GetProgramsWithTitle(title);
                            }
                            else
                                return null;
                        }

                    }
                    else
                        return null;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProgramResponse DeleteProgram(Guid programID)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var program = dataHelper.GetProgramWithId(programID);

                            crudHelper.DeleteProgram(program);

                            return new ProgramResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, Program = null };
                        }
                        else
                            return new ProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                    }
                    else
                        return new ProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                }
                else
                    return new ProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Program = null };
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public ProgramResponse DeactivateProgram(Guid programID)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var program = dataHelper.GetProgramWithId(programID);

                            crudHelper.DeactivateProgram(program);

                            return new ProgramResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, Program = null };
                        }
                        else
                            return new ProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                    }
                    else
                        return new ProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                }
                else
                    return new ProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Program = null };
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public AllProgramResponse GetAllPrograms(bool isHomePage, bool isViewSorted)
        {
            try
            {
                var response = new AllProgramResponse();
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user != null)
                        {
                            var programs = dataHelper.GetAllPrograms();

                            foreach (var program in programs)
                            {
                                var comments = GetComments(new GetCommentRequest() { CommentTarget = (int)CommentTarget.Workout, ProgWorkoutID = program.ID });
                                var registeredUsers = GetRegisteredUsernames(program.ID);
                                var rate = dataHelper.GetRateByID(program.ID);
                                double calculatedRate = CalculateRate(rate);

                                response.Program.Add(new ProgramDetail() { Id = program.ID, Creator = user.Username, Information = program.Information, Rate = calculatedRate, RegisteredUsers = registeredUsers, Subtitle = program.Subtitle, Title = program.Title, ViewCount = program.ViewCount, Category = Enum.GetName(typeof(ProgramCategory), program.ProgramCategory) });
                            }
                            if (isHomePage)
                                response.Program = response.Program.OrderByDescending(_ => _.Rate).Take(10).ToList();
                            if (isViewSorted)
                                response.Program = response.Program.OrderByDescending(_ => _.ViewCount).Take(10).ToList();

                            response.Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" };
                            return response;
                        }
                        else
                            return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                    }
                    else
                        return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Program = null };
                }
                else
                    return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Program = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AllProgramResponse GetAllProgramsByWeightLoss()
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        if (dataHelper.IsSessionActive(ID))
                        {
                            var programs = GetAllPrograms(true, false);
                            var tmpList = new List<ProgramDetail>();
                            foreach (var program in programs.Program)
                            {
                                var weightLossLog = dataHelper.GetWeightLossByProgram(program.Id);
                                var tmpWeightLoss = 0.0;
                                foreach (var log in weightLossLog)
                                {
                                    tmpWeightLoss += log.WeightLoss;
                                }
                                var weightLossResult = tmpWeightLoss / weightLossLog.Count;
                                program.AvarageWeightLoss = weightLossResult;
                                tmpList.Add(program);
                            }
                            return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, Program = tmpList.OrderByDescending(_ => _.AvarageWeightLoss).ToList() };
                        }
                        else
                            return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };
                    }
                    else
                        return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new AllProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AddToProgramResponse AddWorkoutToProgram(AddToProgramRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user.Role == (int)UserRole.Trainer || user.Role == (int)UserRole.Admin)
                        {
                            var relation = new PWRelation();
                            relation.ID = Guid.NewGuid();
                            relation.ProgramID = request.ProgramID;
                            relation.WorkoutID = request.WorkoutID;
                            relation.Queue = request.Queue;
                            relation.IsActive = 1;

                            if (crudHelper.Update<PWRelation>(relation))
                                return new AddToProgramResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                            else
                                return new AddToProgramResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "Failed" } };
                        }
                        else
                            return new AddToProgramResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Permisson is denied for this process" } };
                    }
                    else
                        return new AddToProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new AddToProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JoinToProgramResponse JoinToProgram(JoinToProgramRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user.Role == (int)UserRole.EndUser)
                        {
                            var relation = new UPRelation();
                            relation.ID = Guid.NewGuid();
                            relation.ProgramID = request.ProgramID;
                            relation.UserID = user.ID;
                            relation.RegisterDate = DateTime.Now;
                            relation.IsActive = 1;

                            if (crudHelper.Add<UPRelation>(relation) && crudHelper.SetProgramAsNonEditable(request.ProgramID))
                                return new JoinToProgramResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                            else
                                return new JoinToProgramResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "Join to program failed" } };
                        }
                        else
                            return new JoinToProgramResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Permisson is denied for this process" } };
                    }
                    else
                        return new JoinToProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new JoinToProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JoinToProgramResponse JoinToProgram(Guid programId)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);

                        if (user.Role == (int)UserRole.EndUser)
                        {
                            var relation = new UPRelation();
                            relation.ID = Guid.NewGuid();
                            relation.ProgramID = programId;
                            relation.UserID = user.ID;
                            relation.RegisterDate = DateTime.Now;
                            relation.IsActive = 1;

                            if (crudHelper.Add<UPRelation>(relation) && crudHelper.SetProgramAsNonEditable(programId))
                                return new JoinToProgramResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                            else
                                return new JoinToProgramResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "Join to program failed" } };
                        }
                        else
                            return new JoinToProgramResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Permisson is denied for this process" } };
                    }
                    else
                        return new JoinToProgramResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new JoinToProgramResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActivateProgram(Guid programId)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var user = dataHelper.GetUser(ID);
                        if (dataHelper.IsSessionActive(ID))
                        {
                            var program = dataHelper.GetProgramWithId(programId);
                            program.IsActive = 1;
                            program.IsEditable = 1;
                            program.IsDeleted = 0;

                            if (crudHelper.UpdateProgram(program))
                                return true;
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Comment Processes
        public AddCommentResponse AddComment(AddCommentRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    if (!string.IsNullOrEmpty(request.Message))
                    {
                        Guid ID;
                        if (Guid.TryParse(cookie.Value, out ID))
                        {
                            var user = dataHelper.GetUser(ID);

                            if (user.Role == (int)UserRole.EndUser || user.Role == (int)UserRole.Trainer)
                            {
                                if (request.CommentTarget == (int)CommentTarget.Workout)
                                {
                                    var comment = new Comments() { ID = Guid.NewGuid(), UserID = user.ID, Comment = request.Message, LikeCount = 0, ProgramID = request.ProgWorkoutID, IsActive = 1, CreateDate = DateTime.Now };

                                    if (crudHelper.Add<Comments>(comment))
                                        return new AddCommentResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                                    else
                                        return new AddCommentResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "Failed" } };
                                }
                                else if (request.CommentTarget == (int)CommentTarget.Program)
                                {
                                    var comment = new Comments() { ID = Guid.NewGuid(), UserID = user.ID, Comment = request.Message, LikeCount = 0, ProgramID = request.ProgWorkoutID, IsActive = 1, CreateDate = DateTime.Now };

                                    if (crudHelper.Add<Comments>(comment))
                                        return new AddCommentResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                                    else
                                        return new AddCommentResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "Failed" } };
                                }
                                else
                                    return new AddCommentResponse() { Response = new BaseResponse() { ResponseCode = -6, ResponseMessage = "Corrupt comment target" } };
                            }
                            else
                                return new AddCommentResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Permisson is denied for this process" } };
                        }
                        else
                            return new AddCommentResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                    }
                    else
                    {
                        return new AddCommentResponse() { Response = new BaseResponse() { ResponseCode = -7, ResponseMessage = "Comment is Null" } };
                    }
                }
                else
                    return new AddCommentResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CommentListResponse GetComments(GetCommentRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var commentList = dataHelper.GetCommentList(request.ProgWorkoutID, request.CommentTarget);

                        if (commentList != null)
                        {
                            List<CommentResponse> result = new List<CommentResponse>();
                            foreach (var comment in commentList)
                            {
                                result.Add(new CommentResponse() { Comment = comment.Comment, LikeCount = (int)comment.LikeCount, Username = dataHelper.GetUsername(comment.UserID), CommentID = comment.ID });
                            }

                            return new CommentListResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, Comments = result };
                        }
                        else
                            return new CommentListResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Comment Not Found" }, Comments = null };
                    }
                    else
                        return new CommentListResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Comments = null };
                }
                else
                    return new CommentListResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Comments = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CommentListResponse GetCommentsByAdmin(GetCommentRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SIDA"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var commentList = dataHelper.GetCommentList(request.ProgWorkoutID, request.CommentTarget);

                        if (commentList != null)
                        {
                            List<CommentResponse> result = new List<CommentResponse>();
                            foreach (var comment in commentList)
                            {
                                result.Add(new CommentResponse() { Comment = comment.Comment, LikeCount = (int)comment.LikeCount, Username = dataHelper.GetUsername(comment.UserID), CommentID = comment.ID });
                            }

                            return new CommentListResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, Comments = result };
                        }
                        else
                            return new CommentListResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Comment Not Found" }, Comments = null };
                    }
                    else
                        return new CommentListResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, Comments = null };
                }
                else
                    return new CommentListResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, Comments = null };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DeleteCommentResponse DeleteComment(DeleteCommentRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var session = dataHelper.GetSession(ID);
                        var user = dataHelper.GetUserByID(session.UserID);

                        if (user.Role == (int)UserRole.Admin || user.Role == (int)UserRole.Trainer)
                        {
                            if (crudHelper.DeleteComment(request.CommentID))
                                return new DeleteCommentResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                            else
                                return new DeleteCommentResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "Failed : DB fail" } };
                        }
                        else
                        {
                            return new DeleteCommentResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Failed : You dont have permission" } };
                        }
                    }
                    else
                        return new DeleteCommentResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new DeleteCommentResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Rate Processes
        public AddRateResponse AddRate(AddRateRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];

                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var rate = dataHelper.GetRateByID(request.ProgWorkoutID);

                        switch (request.Rate)
                        {
                            case 1: { rate.One++; break; }
                            case 2: { rate.Two++; break; }
                            case 3: { rate.Three++; break; }
                            case 4: { rate.Four++; break; }
                            case 5: { rate.Five++; break; }
                            default:
                                return new AddRateResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Rate value is Corrupt" } };
                        }

                        if (crudHelper.Update<Rates>(rate))
                        {
                            var unCalculatedRate = dataHelper.GetRateByID(request.ProgWorkoutID);
                            var calculatedRate = CalculateRate(unCalculatedRate);
                            var program = dataHelper.GetProgramWithId(request.ProgWorkoutID);

                            program.Rate = calculatedRate;

                            if (crudHelper.Update<Programs>(program))
                                return new AddRateResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                            else
                                return new AddRateResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "DB save failed : Programs" } };
                        }
                        else
                            return new AddRateResponse() { Response = new BaseResponse() { ResponseCode = -5, ResponseMessage = "DB save failed : Rates" } };
                    }
                    else
                        return new AddRateResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new AddRateResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Common Function
        private double CalculateRate(Rates rate)
        {
            if (rate.One != 0 && rate.Two != 0 && rate.Three != 0 && rate.Four != 0 && rate.Five != 0)
                return ((5 * rate.Five) + (4 * rate.Four) + (3 * rate.Three) + (2 * rate.Two) + (rate.One)) / (rate.Five + rate.Four + rate.Three + rate.Two + rate.One);
            else
                return 0.0;
        }
        private List<string> GetRegisteredUsernames(Guid ID)
        {
            var usernames = new List<string>();
            var relations = dataHelper.GetRelationsByProgramID(ID);

            foreach (var relation in relations)
            {
                var user = dataHelper.GetUserByID(relation.UserID);
                if (user != null)
                    usernames.Add(user.Username);
            }

            return usernames;
        }

        private bool SetProgramStatusNonEditable(Guid ProgramId)
        {
            try
            {
                return crudHelper.SetProgramAsNonEditable(ProgramId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
