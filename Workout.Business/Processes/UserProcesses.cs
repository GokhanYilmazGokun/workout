﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Workout.Business.Models.Enums;
using Workout.Business.Models.RequestModels;
using Workout.Business.Models.ResponseModels;
using Workout.Data;
using Workout.Data.Processes;


namespace Workout.Business.Processes
{
    public class UserProcesses
    {
        private CRUDProcesses crudHelper = new CRUDProcesses();
        private DataProcesses dataHelper = new DataProcesses();
        private WorkoutProcesses workoutProcesses = new WorkoutProcesses();

        #region Register, Login , Logout
        public UserRegisterResponse Register(UserRequests request)
        {
            try
            {
                var user = new Users() { ID = Guid.NewGuid(), Username = request.Username, Password = request.Password, Birthdate = DateTime.ParseExact(request.Birthdate, "mm/dd/yyyy", CultureInfo.InvariantCulture), CreationDate = DateTime.Now, Email = request.Email, Gender = (byte)request.Gender, IsActive = 0, Name = request.Name, Phone = null, Role = 2, Surname = request.Surname, SecurityQuestion = request.Question, SecurityAnswer = request.Answer };

                var workoutRequest = new WorkoutRequest() { Name = "Rest", Category = (int)WorkoutCategory.Rest, Class = (int)WorkoutClass.Rest, Information = "Rest of Training", Subtitle = "Rest of Training", Title = "Rest of Training", Video = "rest.mp4" };
                if (!dataHelper.IsUserExist(user) && workoutProcesses.CreateDefaultWorkout(workoutRequest, user.ID).Response.ResponseCode == 1)
                {
                    if (crudHelper.Add<Users>(user))
                        return new UserRegisterResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "User created" }, UserId = user.ID };
                    else
                        return new UserRegisterResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "User creation failed" }, UserId = null };
                }
                else
                    return new UserRegisterResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "User already exist. Try to login" }, UserId = null };
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public UserRegisterResponse UpdateUser(UserUpdateRequests request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var session = dataHelper.GetSession(ID);
                        var user = dataHelper.GetUserByID(session.UserID);

                        user.Username = request.Username;
                        user.Name = request.Name;
                        user.Birthdate = request.Birthdate;
                        user.Surname = request.Surname;
                        user.Email = request.Email;
                        user.Gender = (byte)request.Gender;
                        user.SecurityQuestion = request.Question;
                        user.SecurityAnswer = request.Answer;
                        user.TrainerInfo = request.Information;

                        if (!dataHelper.IsUserExist(user))
                        {
                            return new UserRegisterResponse()
                            {
                                Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "User not found" },
                                UserId = null
                            };
                        }
                        else
                        {
                            if (crudHelper.UpdateUserProfile(user))
                            {
                                return new UserRegisterResponse()
                                {
                                    Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" },
                                    UserId = null
                                };
                            }
                            else
                            {
                                return new UserRegisterResponse()
                                {
                                    Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Update Failed" },
                                    UserId = null
                                };
                            }
                        }
                    }
                    else
                        return null;
                }
                else
                    return null;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public UserLoginResponse Login(LoginRequest request)
        {
            try
            {
                var user = dataHelper.GetUser(request.Email, request.Password);

                if (user != null)
                {
                    if (user.IsActive == 1)
                    {
                        var Session = new Sessions();
                        Session.ID = Guid.NewGuid();
                        Session.IP = HttpContext.Current.Request.UserHostAddress;
                        Session.UserAgent = HttpContext.Current.Request.UserAgent;
                        Session.UserID = user.ID;
                        Session.LoginDate = DateTime.Now;
                        if (crudHelper.Add<Sessions>(Session))
                        {
                            HttpContext.Current.Session.Add("SID", Session.ID);
                            var sessionCookie = new HttpCookie("SID", Session.ID.ToString());
                            sessionCookie.Expires = DateTime.Now.AddMinutes(45); //15 dakikalık login koruma
                            HttpContext.Current.Response.Cookies.Add(sessionCookie);
                            return new UserLoginResponse()
                            {
                                Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Login Success" },
                                Role = user.Role
                            };
                        }
                        else
                        {
                            return new UserLoginResponse()
                            {
                                Response =
                                    new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session Creation Failed" }
                            };
                        }
                    }
                    else
                    {
                        return new UserLoginResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Your Account is not Activated Please contact with Administrator" } };
                    }
                }
                else
                    return new UserLoginResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login Failed" } };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Users GetCurrentEnduser()
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var session = dataHelper.GetSession(ID);
                        return dataHelper.GetUserByID(session.UserID);
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserLoginResponse CheckLogin()
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    //Session cookie aktif logout olup olmadıgı kontrol edilir.
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        if (dataHelper.IsSessionActive(ID))
                            return new UserLoginResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "User Logged" } };
                        else
                            return new UserLoginResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };
                    }
                    else
                        return new UserLoginResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                {
                    //Cookie yok login olması gerekir.
                    return new UserLoginResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserLogoutResponse Logout()
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;

                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var Session = dataHelper.GetSession(ID);

                        if (Session != null)
                        {
                            Session.LogoutDate = DateTime.Now;
                            crudHelper.Update<Sessions>(Session);
                            HttpContext.Current.Response.Cookies["SID"].Expires = DateTime.Now.AddDays(-1);
                            HttpContext.Current.Session.Abandon();
                            return new UserLogoutResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Logged out" } };
                        }
                        else
                            return new UserLogoutResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Already logged out" } };
                    }
                    else
                        return new UserLogoutResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session Id is corrupt" } };
                }
                else
                    return new UserLogoutResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Already logged out" } };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SecurityAnswerResult CheckSequrityAnswer(SecurityAnswerRequest securityAnswer)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;

                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        var Session = dataHelper.GetSession(ID);

                        if (Session != null)
                        {
                            var user = dataHelper.GetUserByID(Session.UserID);

                            if (securityAnswer.SecurityAnswer == user.SecurityAnswer)
                            {
                                return new SecurityAnswerResult() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Answer Correct" } };
                            }
                            else
                            {
                                return new SecurityAnswerResult() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Answer is Wrong" } };
                            }
                        }
                        else
                            return new SecurityAnswerResult() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Already logged out" } };
                    }
                    else
                        return new SecurityAnswerResult() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session Id is corrupt" } };
                }
                else
                    return new SecurityAnswerResult() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Already logged out" } };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetSecurityQuestion(string email)
        {
            try
            {
                return dataHelper.GetSecurityQuestionOfUser(email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SecurityAnswerQuestionCheck(string question, string answer)
        {
            try
            {
                return dataHelper.ControlSecurity(question, answer);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetNewPasswordToUser(string password)
        {
            try
            {
                return dataHelper.SetNewPassword(password);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Profile Processes

        public ProfileResponse GetProfile()
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        if (dataHelper.IsSessionActive(ID))
                        {
                            var user = dataHelper.GetUser(ID);
                            if (user != null)
                                return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, User = new ProfileDetail() { Name = user.Name, Gender = user.Gender, Height = user.Height, Phone = user.Phone, Surname = user.Surname, Weight = user.Weight } };
                            else
                                return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, User = null };
                        }
                        else
                            return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, User = null };
                    }
                    else
                        return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, User = null };
                }
                else
                    return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, User = null };

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ProfileResponse UpdateProfile(UpdateProfileRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        if (dataHelper.IsSessionActive(ID))
                        {
                            var user = dataHelper.GetUser(ID);

                            user.Name = request.Name;
                            user.Surname = request.Surname;
                            user.Phone = request.Phone;
                            user.Gender = request.Gender;
                            user.Height = request.Height;
                            user.Weight = request.Weight;

                            if (crudHelper.Update<Users>(user))
                                return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, User = null };
                            else
                                return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Update failed" }, User = null };
                        }
                        else
                            return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, User = null };
                    }
                    else
                        return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, User = null };
                }
                else
                    return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, User = null };

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ProfileResponse UpdateHeightWeight(HeightWeightUpdateRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        if (dataHelper.IsSessionActive(ID))
                        {
                            var user = dataHelper.GetUser(ID);
                            var weightLoss = user.Weight - request.Weight;

                            user.Height = request.Height;
                            user.Weight = request.Weight;

                            var weightLog = new UserWeightChangeLog()
                            {
                                ID = Guid.NewGuid(),
                                WeightLoss = weightLoss,
                                RecordDate = DateTime.Now,
                                UserId = user.ID,
                                ProgramID = request.ProgramId,
                                FatRatio = request.FatRatio,
                                MuscleRatio = request.MuscleRatio
                            };

                            if (crudHelper.Update<Users>(user) && crudHelper.Add<UserWeightChangeLog>(weightLog))
                                return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, User = null };
                            else
                                return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Update failed" }, User = null };
                        }
                        else
                            return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, User = null };
                    }
                    else
                        return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" }, User = null };
                }
                else
                    return new ProfileResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" }, User = null };

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public TrainerViewResponse TrainerViewUpdate(TrainerViewRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        if (dataHelper.IsSessionActive(ID))
                        {
                            var user = dataHelper.GetUserByID(request.TrainerID);
                            user.ViewCount++;

                            if (crudHelper.Update<Users>(user))
                                return new TrainerViewResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                            else
                                return new TrainerViewResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Update failed" } };
                        }
                        else
                            return new TrainerViewResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };
                    }
                    else
                        return new TrainerViewResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new TrainerViewResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public AddCertificationResponse AddCertification(AddCertificateRequest request)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        if (dataHelper.IsSessionActive(ID))
                        {
                            var session = dataHelper.GetSession(ID);
                            var user = dataHelper.GetUserByID(session.UserID);

                            var certificate = new Certificates();
                            certificate.ID = Guid.NewGuid();
                            certificate.UserId = user.ID;
                            certificate.IsActive = 1;
                            certificate.ExpireDate = request.ExpireDate;
                            certificate.StartDate = request.StartDate;
                            certificate.CertificateName = request.CertificateName;
                            certificate.CertificateNumber = request.CertififcationNumber;
                            certificate.Filename = request.Filename;

                            if (crudHelper.Add<Certificates>(certificate))
                                return new AddCertificationResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                            else
                                return new AddCertificationResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Insert failed" } };
                        }
                        else
                            return new AddCertificationResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };
                    }
                    else
                        return new AddCertificationResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new AddCertificationResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DeleteCertificationResponse DeleteCertification(string certificationNumber)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        if (dataHelper.IsSessionActive(ID))
                        {
                            if (crudHelper.DeleteCertificate(certificationNumber))
                                return new DeleteCertificationResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" } };
                            else
                                return new DeleteCertificationResponse() { Response = new BaseResponse() { ResponseCode = -4, ResponseMessage = "Delete failed" } };
                        }
                        else
                            return new DeleteCertificationResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };
                    }
                    else
                        return new DeleteCertificationResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new DeleteCertificationResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ProfileListResponse GetProfileListByViewCount()
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["SID"];
                if (cookie != null)
                {
                    Guid ID;
                    if (Guid.TryParse(cookie.Value, out ID))
                    {
                        if (dataHelper.IsSessionActive(ID))
                        {
                            var users = dataHelper.GetTopViewedUsers(10);

                            var userList = new List<ProfileDetail>();

                            foreach (var user in users)
                            {
                                userList.Add(new ProfileDetail() { Name = user.Name, Gender = user.Gender, Height = user.Height, Phone = user.Phone, Surname = user.Surname, ViewCount = user.ViewCount, Weight = user.Weight });
                            }

                            return new ProfileListResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, UserList = userList };
                        }
                        else
                            return new ProfileListResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };
                    }
                    else
                        return new ProfileListResponse() { Response = new BaseResponse() { ResponseCode = -3, ResponseMessage = "Session id is Corrupt" } };
                }
                else
                    return new ProfileListResponse() { Response = new BaseResponse() { ResponseCode = -2, ResponseMessage = "Login is required. Session not found" } };

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public UserListResponse GetDeactiveProfiles()
        {
            try
            {
                return new UserListResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, UserList = dataHelper.GetDeactiveUsers() };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActivateUser(Guid userID)
        {
            try
            {
                return dataHelper.ActivateUser(userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Certificates> GetCertificatesForUser(Guid userId)
        {
            try
            {
                return dataHelper.GetCertificatesForUSer(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid GetUserIdByUsername(string username)
        {
            try
            {
                return dataHelper.GetUserIdWithUsername(username);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Users GetUserByUsername(string username)
        {
            try
            {
                return dataHelper.GetUserByUsername(username);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Users> GetTrainerByUsername(string username)
        {
            try
            {
                return dataHelper.GetTrainerByUsername(username);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Tuple<Guid, int, int>> GetTrainersProgramAndCommentCounts(string username)
        {
            try
            {
                return dataHelper.GetTrainerProgramsCommentsCount(username);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Tuple<Guid, int>> GetRegisteredUsersOfProgram(Guid trainerId)
        {
            try
            {
                return dataHelper.GetRegisteredUsersTuplesOfProgram(trainerId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Users> GetAllTrainers()
        {
            try
            {
                return dataHelper.GetAllTrainers();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        public List<Certificates> GetCertificatesForAllUsers()
        {
            try
            {
                return dataHelper.GetCertificatesForAllUsers();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GetWeightForUserResponse GetWeightForUser(GetWeightForUserRequest req)
        {
            try
            {
                var userWeightLogs = dataHelper.GetUserWeight(req.SessionId);
                var log = userWeightLogs.Select(i => new WeightLog()
                {
                    FatRatio = i.FatRatio.Value,
                    LogDate = i.RecordDate.ToString("dd-MM-yyyy"),
                    MuscleRatio = i.MuscleRatio.Value,
                    SessionId = i.ID,
                    Weight = i.WeightLoss
                }).ToList();

                if (req.ProgramId == "0")
                {
                    log = userWeightLogs.Where(_ => _.ProgramID == new Guid(req.ProgramId)).Select(i => new WeightLog()
                    {
                        FatRatio = i.FatRatio.Value,
                        LogDate = i.RecordDate.ToString("dd-MM-yyyy"),
                        MuscleRatio = i.MuscleRatio.Value,
                        SessionId = i.ID,
                        Weight = i.WeightLoss
                    }).ToList();
                }

                return new GetWeightForUserResponse()
                {
                    Response = new BaseResponse()
                    {
                        ResponseCode = 1,
                        ResponseMessage = "Success"
                    },
                    WeightLogs = log
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public GetWeightForUserResponse GetWeightForProgram(GetWeightForProgramRequest req)
        {
            try
            {
                var userWeightLogs = dataHelper.GetUserWeightByUserId(req.UserId,req.ProgramId);
                var log = userWeightLogs.Select(i => new WeightLog()
                {
                    FatRatio = i.FatRatio.Value,
                    LogDate = i.RecordDate.ToString("dd-MM-yyyy"),
                    MuscleRatio = i.MuscleRatio.Value,
                    SessionId = i.ID,
                    Weight = i.WeightLoss
                }).ToList();

                return new GetWeightForUserResponse()
                {
                    Response = new BaseResponse()
                    {
                        ResponseCode = 1,
                        ResponseMessage = "Success"
                    },
                    WeightLogs = log
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public SetWeightForUserResponse SetWeightForUser(SetWeightForUserRequest req)
        {
            try
            {
                string programId;
                if (req.ProgramId != null)
                    programId = req.ProgramId;
                else
                    programId = "0";
                double weight = crudHelper.SetUserWeight(req.SessionId, req.Weight, req.FatRatio, req.MuscleRatio, programId);

                if (weight > 0)
                {
                    return new SetWeightForUserResponse() { Response = new BaseResponse() { ResponseCode = 1, ResponseMessage = "Success" }, Weight = weight };
                }
                else
                {
                    return new SetWeightForUserResponse() { Response = new BaseResponse() { ResponseCode = -1, ResponseMessage = "Failed" }, Weight = weight };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool BanTrainer(Guid userId)
        {
            try
            {
                return crudHelper.BanTrainer(userId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool BanProgram(Guid programId)
        {
            try
            {
                return crudHelper.BanProgram(programId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool ActivateBannedProgram(Guid programId)
        {
            try
            {
                return crudHelper.ActivateBannedProgram(programId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool BanUser(Guid userId)
        {
            try
            {
                return crudHelper.BanUser(userId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public UserWeightChangeLog GetWeightLog(Guid userId, Guid? programId)
        {
            try
            {
                return dataHelper.GetUserWeightByUserId(userId, programId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Programs> GetMyPrograms(Guid userId)
        {
            try
            {
                var programs = dataHelper.GetProgramListForEndUser(userId);
                return programs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

    }
}
