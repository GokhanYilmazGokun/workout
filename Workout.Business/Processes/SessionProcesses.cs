﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Workout.Business.Models.Enums;
using Workout.Business.Models.ResponseModels;
using Workout.Data;
using Workout.Data.Processes;

namespace Workout.Business.Processes
{
    public class SessionProcesses
    {
        private DataProcesses dataHelper = new DataProcesses();
        private CRUDProcesses crudHelper = new CRUDProcesses();

        public BaseResponse GetAdminLoginResponse(string username, string password)
        {
            try
            {
                var user = dataHelper.GetCoreUser(username);

                if (user != null)
                {
                    if (user.Password == password)
                    {
                        Sessions Session = new Sessions();
                        Session.ID = Guid.NewGuid();
                        Session.LoginDate = DateTime.Now;
                        Session.UserID = user.ID;
                        Session.UserAgent = HttpContext.Current.Request.UserAgent;
                        Session.IP = HttpContext.Current.Request.UserHostAddress;

                        var resp = SessionSave(Session);

                        if (resp.ResponseCode == -1)
                            return new BaseResponse() { ResponseCode = -4, ResponseMessage = "Session Creation Failed" };

                        HttpCookie sessionCookie = new HttpCookie("SIDA", Session.ID.ToString());
                        sessionCookie.Expires = DateTime.Now.AddHours(1);
                        HttpContext.Current.Response.Cookies.Add(sessionCookie);


                        return new BaseResponse() { ResponseCode = 1, ResponseMessage = "Login Success" };
                    }
                    else
                        return new BaseResponse() { ResponseCode = -3, ResponseMessage = "Password Not Matched" };
                }
                else
                    return new BaseResponse() { ResponseCode = -2, ResponseMessage = "User Not Found" };
            }
            catch (Exception ex)
            {

                LogProcesses.ErrorLog(HttpContext.Current, ex);
                return new BaseResponse() { ResponseCode = -1, ResponseMessage = "Critical Error: " + ex.Message };
            }
        }
        public BaseResponse GetAdminLogoutResponse(string SId)
        {
            try
            {
                Guid sessionId = Guid.Parse(SId);
                var Session = dataHelper.GetSession(sessionId);

                if (Session != null)
                {
                    Session.LogoutDate = DateTime.Now;
                    var SessionResponse = SessionSave(Session);

                    if (SessionResponse.ResponseCode == 1 || SessionResponse.ResponseCode == 2)
                    {
                        HttpContext.Current.Response.Cookies["SIDA"].Expires = DateTime.Now.AddDays(-1);
                        HttpContext.Current.Session.Abandon();
                        return new BaseResponse() { ResponseCode = 1, ResponseMessage = "Logged out" };
                    }
                    else
                        return new BaseResponse() { ResponseCode = -5, ResponseMessage = SessionResponse.ResponseMessage };
                }
                else
                    return new BaseResponse() { ResponseCode = -4, ResponseMessage = "Session Not Found" };

            }
            catch (Exception ex)
            {
                LogProcesses.ErrorLog(HttpContext.Current, ex);
                HttpContext.Current.Response.Cookies["SIDA"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Session.Abandon();
                return new BaseResponse() { ResponseCode = -3, ResponseMessage = "2nd Critical Error : " + ex.Message };
            }
        }
        public CoreUsers GetCurrentUser(UserRole role)
        {
            try
            {

                if (HttpContext.Current.Request.Cookies["SIDA"] != null && role == UserRole.Admin)
                {
                    Guid sessionId = Guid.Parse(HttpContext.Current.Request.Cookies["SIDA"].Value.ToString());

                    var session = dataHelper.GetSession(sessionId);
                    var user = dataHelper.GetCoreUser(session.UserID);

                    if (user != null)
                        return user;
                    else
                        return null;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                LogProcesses.ErrorLog(HttpContext.Current, ex);
                return null;
            }
        }
        public Users GetCurrentEndUser()
        {
            if (HttpContext.Current.Request.Cookies["SID"] != null)
            {
                Guid sessionId = Guid.Parse(HttpContext.Current.Request.Cookies["SID"].Value.ToString());

                var session = dataHelper.GetSession(sessionId);
                var user = dataHelper.GetUserByID(session.UserID);

                if (user != null)
                    return user;
                else
                    return null;
            }
            else
                return null;
        }
        public BaseResponse SessionSave(Sessions session)
        {
            try
            {
                var original = dataHelper.GetSession(session.ID);

                if (original != null)
                {
                    crudHelper.Update<Sessions>(session);
                    return new BaseResponse() { ResponseCode = 2, ResponseMessage = "Session Updated" };
                }
                else
                {
                    crudHelper.Add<Sessions>(session);
                    return new BaseResponse() { ResponseCode = 1, ResponseMessage = "Session Created" };
                }
            }
            catch (Exception ex)
            {
                LogProcesses.ErrorLog(HttpContext.Current, ex);
                return new BaseResponse() { ResponseCode = -1, ResponseMessage = "Critical Error : " + ex.Message };
            }
        }
    }
}
