﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workout.Data;

namespace Workout.Business.Models.ResponseModels
{
    public class CreateWorkoutResponse
    {
        public BaseResponse Response { get; set; }
    }
    public class UpdateWorkoutResponse
    {
        public BaseResponse Response { get; set; }
    }
    public class JoinToProgramResponse
    {
        public BaseResponse Response { get; set; }
    }
    public class AddToProgramResponse
    {
        public BaseResponse Response { get; set; }
    }
    public class CreateProgramResponse
    {
        public BaseResponse Response { get; set; }
    }
    public class UpdateProgramResponse
    {
        public BaseResponse Response { get; set; }
    }

    public class AddCommentResponse
    {
        public BaseResponse Response { get; set; }
    }

    public class CommentListResponse
    {
        public BaseResponse Response { get; set; }
       public List<CommentResponse> Comments { get; set; }
    }
    public class CommentResponse
    {
        public string Comment { get; set; }
        public string Username { get; set; }
        public int LikeCount { get; set; }
        public Guid CommentID { get; set; }
    }
    public class AddRateResponse
    {
        public BaseResponse Response { get; set; }
    }

    public class WorkoutResponse
    {
        public BaseResponse Response { get; set; }
        public WorkoutDetail Workout { get; set; }
    }
    public class AllWorkoutResponse
    {
        public BaseResponse Response { get; set; }
        public List<WorkoutDetail> Workout { get; set; }
    }
    public class WorkoutDetail
    {
        public string Name { get; set; }
        public string Video { get; set; }
        public string Information { get; set; }
        public List<CommentResponse> Comments { get; set; }
        public double Rate { get; set; }
        public string Creator { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Category { get; set; }
        public string Class { get; set; }
        public int IsActive { get; set; }
        public Guid ID { get; set; }
        
    }
   
    public class ProgramResponse
    {
        public BaseResponse Response { get; set; }
        public ProgramDetail Program { get; set; }
    }
    public class AllProgramResponse
    {
        public AllProgramResponse()
        {
            Program = new List<ProgramDetail>();
        }
        public BaseResponse Response { get; set; }
        public List<ProgramDetail> Program { get; set; }
    }
    public class ProgramDetail
    {
        public Guid Id { get; set; }
        public double Rate { get; set; }
        public string Creator { get; set; }
        public List<string> RegisteredUsers { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Information { get; set; }
        public int ViewCount { get; set; }
        public double AvarageWeightLoss { get; set; }
        public int IsActive { get; set; }
        public string Category { get; set; }
        public string Level { get; set; }
        public int ProgramLevel { get; set; }
        public int ProgramCategory { get; set; }
        public int CommentCount { get; set; }
        public DateTime CreateDate { get; set; }

    }

    public class TrainerViewResponse
    {
        public BaseResponse Response { get; set; }
    }

    public class AddCertificationResponse
    {
        public BaseResponse Response { get; set; }
    }
    public class DeleteCertificationResponse
    {
        public BaseResponse Response { get; set; }
    }

    public class DeleteCommentResponse
    {
        public BaseResponse Response { get; set; }
    }

 



}
