﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workout.Business.Models.ResponseModels
{
    public class TopTenRatedProgramResponse
    {
        public BaseResponse Response { get; set; }
        public List<ProgramDetail> Program { get; set; }
    }

   
}
