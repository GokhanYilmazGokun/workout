﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workout.Business.Models.Enums;
using Workout.Data;

namespace Workout.Business.Models.ResponseModels
{
    public class UserRegisterResponse
    {
        public BaseResponse Response { get; set; }
        public Guid? UserId { get; set; }
    }

    public class UserLoginResponse
    {
        public BaseResponse Response { get; set; }
        public int Role { get; set; }
    }
    public class UserLogoutResponse
    {
        public BaseResponse Response { get; set; }
    }
    public class SecurityAnswerResult
    {
        public BaseResponse Response { get; set; }
    }
    public class ProfileListResponse
    {
        public BaseResponse Response { get; set; }
        public List<ProfileDetail> UserList { get; set; }
    }

    public class UserListResponse
    {
        public BaseResponse Response { get; set; }
        public List<Users> UserList { get; set; }
    }
    public class ProgramListResponseByLostWeight
    {
        public BaseResponse Response { get; set; }
        public List<ProgramDetail> ProgramList { get; set; }
    }

    public class ProfileResponse
    {
        public BaseResponse Response { get; set; }
        public ProfileDetail User { get; set; }
    }
    public class ProfileDetail
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte Gender { get; set; }
        public string Phone { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
        public int ViewCount { get; set; }
    }
    public class GetWeightForUserResponse
    {
        public BaseResponse Response { get; set; }
        public List<WeightLog> WeightLogs { get; set; }
    }
    public class GetWeightForProgramResponse
    {
        public BaseResponse Response { get; set; }
        public WeightLog WeightLog { get; set; }
    }
    public class WeightLog
    {
        public Guid SessionId { get; set; }
        public double Weight { get; set; }
        public double FatRatio { get; set; }
        public double MuscleRatio { get; set; }
        public string LogDate { get; set; }
    }

    public class SetWeightForUserResponse
    {
        public BaseResponse Response { get; set; }
        public double Weight { get; set; }
    }
}
