﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workout.Business.Models.Enums
{
    public enum UserRole
    {
        Admin,
        EndUser,
        Trainer
    }
    public enum Gender
    {
        Male,
        Female
    }
    public enum CommentTarget
    {
        Workout = 1,
        Program = 2
    }
    public enum RateTarget
    {
        Workout = 1,
        Program = 2
    }

    public enum WorkoutCategory
    {
        [Description("Arms Training")]
        ArmsTraining = 0,
        [Description("Back Muscles Training")]
        BackMusclesTraining = 1,
        [Description("Chest Training")]
        ChestTraining = 2,
        [Description("Core Training")]
        CoreTraining = 3,
        [Description("Glutes and Thighs Training")]
        GlutesandThighsTraining = 4,
        [Description("Legs Training")]
        LegsTraining = 5,
        [Description("Shoulder Training")]
        ShoulderTraining = 6,
        [Description("Rest")]
        Rest = 7
    }

    public enum WorkoutClass
    {
        [Description("Body Building")]
        BodyBuilding = 0,
        [Description("Cardio")]
        Cardio = 1,
        [Description("Fitness")]
        Fitness = 2,
        [Description("Flexibility")]
        Flexibility = 3,
        [Description("Rest")]
        Rest = 4

    }

    public enum ProgramLevels
    {
        [Description("Beginner")]
        Beginner = 0,
        [Description("Intermediate")]
        Intermediate = 1,
        [Description("Advanced")]
        Advanced = 2
    }

    public enum ProgramCategory
    {
        [Description("Agility")]
        Agility = 0,
        [Description("Strength")]
        Strength = 1,
        [Description("Fitness")]
        Fitness = 2,
        [Description("Fat Burning")]
        FatBurning = 3
    }

}