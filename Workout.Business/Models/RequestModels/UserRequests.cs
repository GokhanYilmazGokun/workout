﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workout.Business.Models.Enums;

namespace Workout.Business.Models.RequestModels
{
    public class UserRequests
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Birthdate { get; set; }
        public Gender Gender { get; set; }
        public int Role { get; set; }
    }
    public class UserUpdateRequests
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string Email { get; set; }
        public DateTime Birthdate { get; set; }
        public Gender Gender { get; set; }
        public int Role { get; set; }
        public string Information { get; set; }
    }
    public class LoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class SecurityAnswerRequest
    {
        public string SecurityAnswer { get; set; }
    }

    public class UpdateProfileRequest
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte Gender { get; set; }
        public string Phone { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
    }

    public class HeightWeightUpdateRequest
    {
        public double Height { get; set; }
        public double Weight { get; set; }
        public double FatRatio { get; set; }
        public double MuscleRatio { get; set; }
        public Guid ProgramId { get; set; }

    }

    public class GetWeightForUserRequest
    {
        public  Guid SessionId { get; set; }
        public string ProgramId { get; set; }
    }

    public class GetWeightForProgramRequest
    {
        public Guid ProgramId { get; set; }
        public Guid UserId { get; set; }
    }

    public class SetWeightForUserRequest
    {
        public Guid SessionId { get; set; }
        public double Weight { get; set; }
        public double? FatRatio { get; set; }
        public  double? MuscleRatio { get; set; }
        public string ProgramId { get; set; }
    }
    
}
