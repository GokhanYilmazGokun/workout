﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workout.Business.Models.RequestModels
{
    public class WorkoutRequest
    {
        public string Name { get; set; }
        public string Information { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public int Category { get; set; }
        public int Class { get; set; }
        public string Video { get; set; }
    }
    public class UpdateWorkoutRequest
    {
        public Guid WorkoutID { get; set; }
        public string Name { get; set; }
        public string Information { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public int Category { get; set; }
        public int Class { get; set; }
        public string Video { get; set; }
    }

    public class JoinToProgramRequest
    {
        public Guid ProgramID { get; set; }
    }
    public class AddToProgramRequest
    {
        public Guid WorkoutID { get; set; }
        public Guid ProgramID { get; set; }
        public int Queue { get; set; }
    }
    public class CreateProgramRequest
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Information { get; set; }
        public int ProgramCategory { get; set; }
        public int ProgramLevel { get; set; }
        public List<SelectedWorkout> SelectedWorkouts { get; set; }
    }
    public class UpdateProgramRequest
    {
        public Guid ProgramID { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Information { get; set; }
        public int ProgramCategory { get; set; }
        public int ProgramLevel { get; set; }
        public List<SelectedWorkout> SelectedWorkouts { get; set; }
    }
    public class SelectedWorkout
    {
        public int order { get; set; }
        public string workout { get; set; }
        public int period { get; set; }
        public int repeat { get; set; }
        public string relationid { get; set; }
    }

    public class AddCommentRequest
    {
        public Guid ProgWorkoutID { get; set; }
        public string Message { get; set; }
        public int CommentTarget { get; set; }
    }

    public class GetCommentRequest
    {
        public Guid ProgWorkoutID { get; set; }
        public int CommentTarget { get; set; }
    }
    public class AddRateRequest
    {
        public Guid ProgWorkoutID { get; set; }
        public int Rate { get; set; }
    }
    public class GetWorkoutRequest
    {
        public Guid WorkoutID { get; set; }
    }
    public class GetProgramRequest
    {
        public Guid ProgramID { get; set; }
    }
    public class TrainerViewRequest
    {
        public Guid TrainerID { get; set; }
    }
    public class DeleteCommentRequest
    {
        public Guid CommentID { get; set; }
    }

    public class AddCertificateRequest
    {
        public  string CertificateName { get; set; }
        public string CertififcationNumber { get; set; }
        public DateTime StartDate { get; set; }
        public  DateTime ExpireDate { get; set; }
        public string Filename { get; set; }
             
    }

 
}
