﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using Workout.Data.Models;

namespace Workout.Data.Processes
{
    public class DataProcesses
    {
        private WorkoutEntities DataContext = new WorkoutEntities();
        private CRUDProcesses crudHelper = new CRUDProcesses();

        #region User and Session Data Processes
        public bool IsUserExist(Users user)
        {
            return DataContext.Users.Any(_ => _.Email == user.Email || _.Username == user.Username);
        }
        public bool IsUserExist(string email)
        {
            return DataContext.Users.Any(_ => _.Email == email);
        }
        public Users GetUser(string email, string password)
        {
            return DataContext.Users.Where(_ => _.Email == email && _.Password == password).FirstOrDefault();
        }
        public Users GetUserByID(Guid userId)
        {
            return DataContext.Users.Where(_ => _.ID == userId).FirstOrDefault();
        }

        public Users GetUserByUsername(string username)
        {
            return DataContext.Users.FirstOrDefault(_ => _.Username.Contains(username) && _.Role == 1 && _.IsActive == 1);
        }
        public List<Users> GetTrainerByUsername(string username)
        {
            return DataContext.Users.Where(_ => _.Username.Contains(username) && _.Role == 2 && _.IsActive == 1).ToList();
        }
        public List<Tuple<Guid,int,int>> GetTrainerProgramsCommentsCount(string username)
        {
            List<Tuple<Guid,int,int>> result = new List<Tuple<Guid, int, int>>();
            List<Users> trainers = new List<Users>();

            if (!string.IsNullOrEmpty(username))
            {
               trainers = DataContext.Users.Where(_ => _.Username.Contains(username) && _.Role == 2 && _.IsActive == 1).ToList();
            }
            else
            {
                trainers = DataContext.Users.Where(_ => _.Role == 2 && _.IsActive == 1).ToList();
            }

            foreach (var trainer in trainers)
            {
                var programs = DataContext.Programs.Where(_ => _.Creator == trainer.ID && _.IsActive == 1).ToList();
                var comments = new List<Comments>();
                foreach (var program in programs)
                {
                    var commentList = DataContext.Comments.Where(_ => _.ProgramID == program.ID && _.IsActive == 1).ToList();
                    comments.AddRange(commentList);
                }
                result.Add(new Tuple<Guid, int, int>(trainer.ID, programs.Count, comments.Count));
            }
            return result;
        }
        public List<Tuple<Guid, int>> GetRegisteredUsersTuplesOfProgram(Guid trainerId)
        {
            List<Tuple<Guid, int>> result = new List<Tuple<Guid, int>>();
            List<Programs> programs = new List<Programs>();

            programs = DataContext.Programs.Where(_ => _.Creator == trainerId && _.IsActive == 1).ToList();

            foreach (var program in programs)
            {
                var registeredUsers = DataContext.UPRelation.Where(_ => _.ProgramID == program.ID && _.IsActive == 1).ToList();
                
                result.Add(new Tuple<Guid, int>(program.ID,registeredUsers.Count));
            }
            return result;
        }
        public List<Users> GetAllTrainers()
        {
            return DataContext.Users.Where(_ => _.Role == 2 && _.IsActive == 1).ToList();
        }
        public string GetSecurityQuestionOfUser(string email)
        {
            var user = DataContext.Users.FirstOrDefault(_ => _.Email == email);
            HttpContext.Current.Session.Add("UserSecurity", user);
            if (user != null) return user.SecurityQuestion;
            else return string.Empty;
        }

        public bool ControlSecurity(string question, string answer)
        {
            var user = (Users)HttpContext.Current.Session["UserSecurity"];

            if (user != null)
            {
                if (user.SecurityQuestion == question && user.SecurityAnswer == answer)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public bool SetNewPassword(string password)
        {
            var user = (Users)HttpContext.Current.Session["UserSecurity"];

            if (user != null)
            {
                using (SHA1Managed sha1 = new SHA1Managed())
                {
                    var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(password));
                    var sb = new StringBuilder(hash.Length * 2);

                    foreach (byte b in hash)
                    {
                        sb.Append(b.ToString("x2"));
                    }

                    user.Password = sb.ToString();
                }
                return crudHelper.UpdateUser(user);
            }
            else
            {
                return false;
            }
        }
        public Users GetUser(Guid sessionId)
        {
            var session = DataContext.Sessions.Where(_ => _.ID == sessionId).FirstOrDefault();
            if (session != null)
                return DataContext.Users.Where(_ => _.ID == session.UserID).FirstOrDefault();
            else
                return null;
        }
        public string GetUsername(Guid ID)
        {
            var user = DataContext.Users.Where(_ => _.ID == ID).FirstOrDefault();
            return user.Username;
        }

        public Guid GetUserIdWithUsername(string username)
        {
            return DataContext.Users.Where(_ => _.Username == username).Select(_ => _.ID).FirstOrDefault();
        }

        public bool ActivateUser(Guid ID)
        {
            return crudHelper.ActivateUser(ID);
        }

        public List<Certificates> GetCertificatesForUSer(Guid userId)
        {
            return DataContext.Certificates.Where(_ => _.IsActive == 1 && _.ExpireDate > DateTime.Now && _.UserId == userId).ToList();
        }

        public List<Certificates> GetCertificatesForAllUsers()
        {
            return DataContext.Certificates.Where(_ => _.IsActive == 1).ToList();
        }

        public CoreUsers GetCoreUser(string username)
        {
            return DataContext.CoreUsers.Where(_ => _.Username == username).FirstOrDefault();
        }
        public CoreUsers GetCoreUser(Guid ID)
        {
            return DataContext.CoreUsers.Where(_ => _.ID == ID).FirstOrDefault();
        }
        public Sessions GetSession(Guid ID)
        {
            return DataContext.Sessions.Find(ID);
        }

        public bool IsSessionActive(Guid ID)
        {
            var Session = DataContext.Sessions.Where(_ => _.ID == ID).FirstOrDefault();

            if (Session != null)
            {
                if (Session.LogoutDate == null)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public List<Users> GetDeactiveUsers()
        {
            try
            {
                return DataContext.Users.Where(_ => _.IsActive == 0).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserWeightChangeLog> GetUserWeight(Guid SessionId)
        {
            var session = DataContext.Sessions.FirstOrDefault(_ => _.ID == SessionId);
            return
                DataContext.UserWeightChangeLog.Where(_ => _.UserId == session.UserID)
                    .OrderBy(_ => _.RecordDate).ToList();
        }
        public UserWeightChangeLog GetUserWeightByUserId(Guid userId,Guid? programId)
        {
            if (programId != null)
            {
                return DataContext.UserWeightChangeLog.Where(_ => _.UserId == userId && _.ProgramID == programId)
                    .OrderBy(_ => _.RecordDate)
                    .FirstOrDefault();
            }
            else
            {
                return DataContext.UserWeightChangeLog.Where(_ => _.UserId == userId)
                    .OrderBy(_ => _.RecordDate)
                    .FirstOrDefault();
            }
            
        }
        public List<UserWeightChangeLog> GetUserWeightByUserId(Guid userId, Guid programId)
        {
            return
                DataContext.UserWeightChangeLog.Where(_ => _.UserId == userId && _.ProgramID == programId)
                    .OrderBy(_ => _.RecordDate)
                    .ToList();
        }
        #endregion

        #region Workout Program Data Processes
        public List<WorkoutDetailsResponse> GetWorkoutsOfProgram(Guid programID)
        {
            var relation = DataContext.PWRelation.Where(_ => _.ProgramID == programID && _.IsActive == 1).OrderBy(_ => _.Queue).ToList();
            var workoutList = new List<WorkoutDetailsResponse>();

            foreach (var item in relation)
            {
                workoutList.Add(new WorkoutDetailsResponse()
                {
                    RelationId = item.ID,
                    Period = item.Period,
                    Repeat = item.Repeat,
                    Workout = DataContext.Workouts.FirstOrDefault(_ => _.ID == item.WorkoutID && _.IsActive == 1)
                });
            }
            return workoutList;
        }

        public Programs GetProgramWithId(Guid ID)
        {
            return DataContext.Programs.FirstOrDefault(_ => _.ID == ID);
        }

        public List<Programs> GetProgramsWithTitle(string Title)
        {
            List<Programs> list = new List<Programs>();
            foreach (var programs in DataContext.Programs.Where(_ => _.Title.Contains(Title)))
                list.Add(programs);
            return list;
        }

        public List<Users> GetRegisteredUsersOfProgram(Guid programId)
        {
            List<Users> registeredUsers = new List<Users>();
           var UPRelationList = DataContext.UPRelation.Where(_ => _.ProgramID == programId && _.IsActive == 1).ToList();

            foreach (var relation in UPRelationList)
            {
                registeredUsers.Add(GetUserByID(relation.UserID));
            }

            return registeredUsers;
        } 

        public Workouts GetWorkoutWithId(Guid ID)
        {
            return DataContext.Workouts.Where(_ => _.ID == ID && _.IsActive == 1).FirstOrDefault();
        }
        public Workouts GetWorkoutWithIdForUser(Guid userID, Guid workoutID)
        {
            return DataContext.Workouts.Where(_ => _.ID == workoutID && _.IsActive == 1 && _.Creator == userID).FirstOrDefault();
        }
        public List<Programs> GetAllPrograms()
        {
            return DataContext.Programs.Where(_ => _.IsActive == 1).ToList();
        }
        public List<Workouts> GetAllWorkouts()
        {
            return DataContext.Workouts.Where(_ => _.IsActive == 1).ToList();
        }
        public List<Workouts> GetWorkoutListForUser(Guid userID)
        {
            return DataContext.Workouts.Where(_ => _.Creator == userID && _.IsActive == 1).ToList();
        }
        public List<Programs> GetProgramListForUser(Guid userID)
        {
            return DataContext.Programs.Where(_ => _.Creator == userID && _.IsActive == 1 && _.IsDeleted == 0).ToList();
        }
        public List<Programs> GetProgramListForEndUser(Guid userID)
        {
            var relationList = DataContext.UPRelation.Where(_ => _.UserID == userID).OrderByDescending(_=>_.RegisterDate).ToList();
            List<Programs> programList = new List<Programs>();
            foreach (var relation in relationList)
            {
                var program = DataContext.Programs.FirstOrDefault(_ => _.ID == relation.ProgramID && _.IsActive == 1 && _.IsDeleted == 0);
                if(program != null)
                   programList.Add(program);
            }
            return programList;
        }
        public List<Programs> GetInActiveProgramListForUser(Guid userID)
        {
            return DataContext.Programs.Where(_ => _.Creator == userID && _.IsActive == 0 && _.IsDeleted == 0).ToList();
        }
        public List<UPRelation> GetRelationsByProgramID(Guid programID)
        {
            return DataContext.UPRelation.Where(_ => _.ProgramID == programID).ToList();
        }
        public PWRelation GetPWRelation(Guid workoutID, Guid programID, Guid relationid)
        {
            return DataContext.PWRelation.FirstOrDefault(_ => _.WorkoutID == workoutID && _.ProgramID == programID && _.IsActive == 1 && _.ID == relationid);
        }
        public List<PWRelation> GetPWRelationsForDelete(Guid workoutID)
        {
            return DataContext.PWRelation.Where(_ => _.WorkoutID == workoutID && _.IsActive == 1).ToList();
        }
        #endregion

        #region Comment and Rate Data Processes
        public List<Comments> GetCommentList(Guid ID, int Target)
        {
            if (Target == 1)
            {
                //Workout
                return DataContext.Comments.Where(_ => _.WorkoutID == ID && _.IsActive == 1).OrderBy(_=>_.CreateDate).ToList();
            }
            else if (Target == 2)
            {
                //Program
                return DataContext.Comments.Where(_ => _.ProgramID == ID && _.IsActive == 1).OrderBy(_=>_.CreateDate).ToList();
            }
            else
            {
                return null;
            }
        }

        public Comments GetCommentByID(Guid ID)
        {
            return DataContext.Comments.OrderBy(_=>_.CreateDate).FirstOrDefault(_ => _.ID == ID);
        }

        public Rates GetRateByID(Guid ID)
        {
            return DataContext.Rates.FirstOrDefault(_ => _.ID == ID);
        }

        #endregion

        #region Top10 Dashboard Data Processes

        public List<Programs> GetTopPrograms(int count)
        {
            return DataContext.Programs.Where(_ => _.IsActive == 1).OrderByDescending(_ => _.Rate).Take(count).ToList();
        }

        public List<Users> GetTopViewedUsers(int count)
        {
            return DataContext.Users.Where(_ => _.IsActive == 1).OrderByDescending(_ => _.ViewCount).Take(count).ToList();
        }

        public List<UserWeightChangeLog> GetWeightLossByProgram(Guid ProgramId)
        {
            return DataContext.UserWeightChangeLog.Where(_ => _.ProgramID == ProgramId).ToList();
        }

        #endregion

    }
}
