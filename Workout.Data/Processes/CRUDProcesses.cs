﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workout.Data.Processes
{
    public class CRUDProcesses
    {
        private WorkoutEntities DataContext = new WorkoutEntities();

        public bool Add<E>(E entity) where E : class
        {
            DataContext.Entry(entity).State = EntityState.Added;
            return Save();
        }
        public bool Update<E>(E entity) where E : class
        {
            DataContext.Entry(entity).State = EntityState.Modified;
            return Save();
        }
        public bool Delete<E>(E entity) where E : class
        {
            DataContext.Entry(entity).State = EntityState.Deleted;
            return Save();
        }

        private bool Save()
        {
            return DataContext.SaveChanges() > 0;
        }

        public bool AddRelation(Programs program, Workouts workout, PWRelation relation)
        {
            DataContext.Programs.FirstOrDefault(_ => _.ID == program.ID).PWRelation.Add(relation);

            return Save();
        }

        #region Entity Based Functions

        public bool UpdateProgram(Programs program)
        {
            var original = DataContext.Programs.Find(program.ID);
            if (original != null)
            {
                var updated = new Programs();
                updated = original;
                updated.Title = program.Title;
                updated.Subtitle = program.Subtitle;
                updated.Information = program.Information;
                updated.ProgramCategory = program.ProgramCategory;
                updated.ProgramLevel = program.ProgramLevel;
                updated.CreateDate = program.CreateDate;
                updated.IsActive = program.IsActive;
                updated.IsEditable = program.IsEditable;
                updated.IsDeleted = program.IsDeleted;
                DataContext.Entry(original).CurrentValues.SetValues(updated);
                return Save();
            }
            else
                return false;
        }
        public bool UpdatePWRelation(PWRelation relation)
        {
            var original = DataContext.PWRelation.Find(relation.ID);
            if (original != null)
            {
                var updated = new PWRelation();
                updated = original;
                updated.Queue = relation.Queue;
                updated.Period = relation.Period;
                updated.Repeat = relation.Repeat;

                DataContext.Entry(original).CurrentValues.SetValues(updated);
                return Save();
            }
            else
                return false;
        }
        public bool UpdateWorkout(Workouts workout)
        {
            var original = DataContext.Workouts.Find(workout.ID);
            if (original != null)
            {
                var updated = new Workouts();
                updated = original;
                updated.Name = workout.Name;
                updated.Title = workout.Title;
                updated.Subtitle = workout.Subtitle;
                updated.Information = workout.Information;
                updated.Video = workout.Video;
                updated.Category = workout.Category;
                updated.WClass = workout.WClass;

                DataContext.Entry(original).CurrentValues.SetValues(updated);
                return Save();
            }
            else
                return false;
        }
        public bool DeleteWorkout(Workouts workout)
        {
            var originalWorkoutEntity = DataContext.Workouts.Find(workout.ID);
            var originalRelationList = DataContext.PWRelation.Where(_ => _.WorkoutID == workout.ID).ToList();
            if (originalWorkoutEntity != null)
            {
                var updated = new Workouts();
                updated = originalWorkoutEntity;
                updated.IsActive = 0;

                if (originalRelationList.Any())
                {
                    foreach (var originalRelationEntity in originalRelationList)
                    {
                        var updatedRelation = originalRelationEntity;
                        updatedRelation.IsActive = 0;
                        DataContext.Entry(originalRelationEntity).CurrentValues.SetValues(updatedRelation);
                        Save();
                    }
                }

                DataContext.Entry(originalWorkoutEntity).CurrentValues.SetValues(updated);
                return Save();
            }
            else
                return false;
        }
        public bool DeleteProgram(Programs program)
        {
            var original = DataContext.Programs.Find(program.ID);
            var originalRelationList = DataContext.PWRelation.Where(_ => _.ProgramID == program.ID && _.IsActive == 1).ToList();
            var userRelationCount = DataContext.UPRelation.Count(_ => _.ProgramID == program.ID && _.IsActive == 1);

            if (userRelationCount == 0)
            {
                if (original != null)
                {
                    var updated = new Programs();
                    updated = original;
                    updated.IsActive = 0;
                    updated.IsDeleted = 1;
                    updated.IsEditable = 0;

                    if (originalRelationList.Any())
                    {
                        foreach (var originalRelationEntity in originalRelationList)
                        {
                            var updatedRelation = originalRelationEntity;
                            updatedRelation.IsActive = 0;
                            DataContext.Entry(originalRelationEntity).CurrentValues.SetValues(updatedRelation);
                            Save();
                        }
                    }

                    DataContext.Entry(original).CurrentValues.SetValues(updated);
                    return Save();
                }
                else
                    return false;
            }
            else
                return false;

        }
        public bool DeactivateProgram(Programs program)
        {
            var original = DataContext.Programs.Find(program.ID);
            var originalRelationList = DataContext.PWRelation.Where(_ => _.ProgramID == program.ID && _.IsActive == 1).ToList();
            var userRelationCount = DataContext.UPRelation.Count(_ => _.ProgramID == program.ID && _.IsActive == 1);

            if (userRelationCount == 0)
            {
                if (original != null)
                {
                    var updated = new Programs();
                    updated = original;
                    updated.IsActive = 0;
                    updated.IsDeleted = 0;
                    updated.IsEditable = 1;

                    if (originalRelationList.Any())
                    {
                        foreach (var originalRelationEntity in originalRelationList)
                        {
                            var updatedRelation = originalRelationEntity;
                            updatedRelation.IsActive = 0;
                            DataContext.Entry(originalRelationEntity).CurrentValues.SetValues(updatedRelation);
                            Save();
                        }
                    }

                    DataContext.Entry(original).CurrentValues.SetValues(updated);
                    return Save();
                }
                else
                    return false;
            }
            else
                return false;

        }
        public bool ActivateUser(Guid ID)
        {
            var original = DataContext.Users.Find(ID);
            if (original != null)
            {
                var updated = new Users();
                updated = original;
                updated.IsActive = 1;

                DataContext.Entry(original).CurrentValues.SetValues(updated);
                return Save();
            }
            else
                return false;
        }

        public bool DeleteCertificate(string certificateNumber)
        {
            var original =
                DataContext.Certificates.FirstOrDefault(_ => _.CertificateNumber == certificateNumber);
            if (original != null)
            {
                var updated = new Certificates();
                updated = original;
                updated.IsActive = 0;

                DataContext.Entry(original).CurrentValues.SetValues(updated);
                return Save();
            }
            else
                return false;
        }
        public bool DeleteComment(Guid commentID)
        {
            var original =
                DataContext.Comments.FirstOrDefault(_ => _.ID == commentID);
            if (original != null)
            {
                var updated = new Comments();
                updated = original;
                updated.IsActive = 0;

                DataContext.Entry(original).CurrentValues.SetValues(updated);
                return Save();
            }
            else
                return false;
        }
        public bool UpdateUser(Users user)
        {
            var original = DataContext.Users.Find(user.ID);
            if (original != null)
            {
                var updated = new Users();
                updated = original;
                updated.Email = user.Email;
                updated.Password = user.Password;
                updated.SecurityAnswer = user.SecurityAnswer;
                updated.SecurityQuestion = user.SecurityQuestion;
                updated.Birthdate = user.Birthdate;
                updated.Name = user.Name;
                updated.Surname = user.Surname;

                DataContext.Entry(original).CurrentValues.SetValues(updated);
                return Save();
            }
            else
                return false;
        }
        public bool UpdateUserProfile(Users user)
        {
            var original = DataContext.Users.Find(user.ID);
            if (original != null)
            {
                var updated = new Users();
                updated = original;
                updated.Email = user.Email;
                updated.SecurityAnswer = user.SecurityAnswer;
                updated.SecurityQuestion = user.SecurityQuestion;
                updated.Birthdate = user.Birthdate;
                updated.Name = user.Name;
                updated.Surname = user.Surname;
                updated.Username = user.Username;
                updated.TrainerInfo = user.TrainerInfo;
                DataContext.Entry(original).CurrentValues.SetValues(updated);
                return Save();
            }
            else
                return false;
        }

        public bool SetProgramAsNonEditable(Guid programID)
        {
            var original = DataContext.Programs.Find(programID);
            if (original != null)
            {
                var updated = new Programs();
                updated = original;
                updated.IsEditable = 0;
                DataContext.Entry(original).CurrentValues.SetValues(updated);
                return Save();
            }
            else
                return false;
        }

        public double SetUserWeight(Guid SessionId, double Weight, double? fatRatio, double? muscleRatio, string programId)
        {
            Guid programGuid = Guid.Empty;
            if (programId != "0")
                programGuid = new Guid(programId);

            var session = DataContext.Sessions.FirstOrDefault(_ => _.ID == SessionId);

            var original = DataContext.Users.Find(session.UserID);

            if (original != null)
            {
                var updated = new Users();
                updated = original;
                updated.Weight = Weight;
                var wLog = new UserWeightChangeLog();
                wLog.ID = Guid.NewGuid();
                wLog.FatRatio = fatRatio;
                wLog.MuscleRatio = muscleRatio;
                wLog.ProgramID = Guid.Empty;
                wLog.RecordDate = DateTime.Now;
                wLog.UserId = session.UserID;
                wLog.WeightLoss = Weight;
                wLog.ProgramID = programGuid;

                Add(wLog);

                DataContext.Entry(original).CurrentValues.SetValues(updated);

                Save();

                return Weight;
            }
            return 0.0;
        }

        public bool BanUser(Guid userId)
        {
            var original = DataContext.Users.Find(userId);

            if (original != null)
            {
                var updated = new Users();
                updated = original;
                updated.IsActive = 0;

                DataContext.Entry(original).CurrentValues.SetValues(updated);

                return Save();
            }
            else
                return false;
        }
        public bool BanTrainer(Guid userId)
        {
            var original = DataContext.Users.Find(userId);
            var programs = DataContext.Programs.Where(_ => _.Creator == userId).ToList();

            if (original != null)
            {
                var updated = new Users();
                updated = original;
                updated.IsActive = 0;

                DataContext.Entry(original).CurrentValues.SetValues(updated);
                foreach (var program in programs)
                {
                    DeleteProgram(program);
                }
                return Save();
            }
            else
                return false;
        }

        public bool BanProgram(Guid programId)
        {
            var original = DataContext.Programs.Find(programId);

            if (original != null)
            {
                var updated = new Programs();
                updated = original;
                updated.IsActive = 0;
                updated.IsDeleted = 1;
                updated.IsEditable = 0;

                DataContext.Entry(original).CurrentValues.SetValues(updated);

                return Save();
            }
            else
                return false;
        }

        public bool ActivateBannedProgram(Guid programId)
        {
            var original = DataContext.Programs.Find(programId);

            if (original != null)
            {
                var updated = new Programs();
                updated = original;
                updated.IsActive = 1;
                updated.IsDeleted = 0;
                updated.IsEditable = 1;

                DataContext.Entry(original).CurrentValues.SetValues(updated);

                return Save();
            }
            else
                return false;
        }
        #endregion

    }
}
