﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Workout.Data.Processes
{
    public static class LogProcesses
    {
        private static WorkoutEntities DataContext = new WorkoutEntities();
        public static void ErrorLog(HttpContext context, Exception Eex)
        {
            try
            {
                DataContext.ErrorLogs.Add(new ErrorLogs() { Id = Guid.NewGuid(), ErrorMessage = Eex.Message, InnerException = Eex.InnerException == null ? "-" : Eex.InnerException.Message, Url = context.Request.Url.ToString(), Page = context.Request.Path, Ip = context.Request.UserHostAddress, CreateDate = DateTime.Now, UserId = context.Request.Cookies["SID"] == null ? new Guid() : Guid.Parse(context.Request.Cookies["SID"].Value) });
                DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                DataContext.ErrorLogs.Add(new ErrorLogs() { Id = Guid.NewGuid(), ErrorMessage = ex.Message, InnerException = ex.InnerException == null ? "-" : ex.InnerException.Message, Url = HttpContext.Current.Request.Url.ToString(), Page = HttpContext.Current.Request.Path, Ip = HttpContext.Current.Request.UserHostAddress, CreateDate = DateTime.Now, UserId = context.Request.Cookies["SID"] == null ? new Guid() : Guid.Parse(context.Request.Cookies["SID"].Value) });
                DataContext.SaveChanges();
                return;
            }
        }
        public static void ErrorLog(Exception Eex)
        {
            try
            {
                DataContext.ErrorLogs.Add(new ErrorLogs() { Id = Guid.NewGuid(), ErrorMessage = Eex.Message, InnerException = Eex.InnerException == null ? "-" : Eex.InnerException.Message, Url = "Controller", Page = HttpContext.Current.Request.Path, Ip = HttpContext.Current.Request.UserHostAddress, CreateDate = DateTime.Now, UserId = new Guid() });
                DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                DataContext.ErrorLogs.Add(new ErrorLogs() { Id = Guid.NewGuid(), ErrorMessage = ex.Message, InnerException = ex.InnerException == null ? "-" : ex.InnerException.Message, Url = HttpContext.Current.Request.Url.ToString(), Page = HttpContext.Current.Request.Path, Ip = HttpContext.Current.Request.UserHostAddress, CreateDate = DateTime.Now, UserId = new Guid() });
                DataContext.SaveChanges();
                return;
            }
        }
        
    }
}
