﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workout.Data.Models
{
    public class WorkoutDetailsResponse
    {
        public Guid RelationId { get; set; }
        public Workouts Workout { get; set; }
        public int Repeat { get; set; }
        public int Period { get; set; }

    }
}
