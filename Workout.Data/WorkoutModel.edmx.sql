
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 10/31/2016 21:57:34
-- Generated from EDMX file: C:\Users\gokha\OneDrive\documents\visual studio 2015\Projects\Workout.Business\Workout.Data\WorkoutModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Workout];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Comments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Comments];
GO
IF OBJECT_ID(N'[dbo].[CoreUsers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CoreUsers];
GO
IF OBJECT_ID(N'[dbo].[ErrorLogs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ErrorLogs];
GO
IF OBJECT_ID(N'[dbo].[Programs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Programs];
GO
IF OBJECT_ID(N'[dbo].[Rates]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Rates];
GO
IF OBJECT_ID(N'[dbo].[Sessions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sessions];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Workouts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Workouts];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'CoreUsers'
CREATE TABLE [dbo].[CoreUsers] (
    [ID] uniqueidentifier  NOT NULL,
    [Username] nvarchar(50)  NOT NULL,
    [Password] nvarchar(100)  NOT NULL,
    [CreationDate] datetime  NOT NULL
);
GO

-- Creating table 'Sessions'
CREATE TABLE [dbo].[Sessions] (
    [ID] uniqueidentifier  NOT NULL,
    [LoginDate] datetime  NOT NULL,
    [LogoutDate] datetime  NULL,
    [UserAgent] nvarchar(500)  NOT NULL,
    [IP] nvarchar(50)  NOT NULL,
    [UserID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'ErrorLogs'
CREATE TABLE [dbo].[ErrorLogs] (
    [Id] uniqueidentifier  NOT NULL,
    [ErrorMessage] nvarchar(max)  NULL,
    [InnerException] nvarchar(max)  NULL,
    [Url] nvarchar(max)  NULL,
    [Page] nvarchar(max)  NULL,
    [Ip] nvarchar(50)  NULL,
    [CreateDate] datetime  NOT NULL,
    [UserId] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [ID] uniqueidentifier  NOT NULL,
    [Username] nvarchar(50)  NOT NULL,
    [Password] nvarchar(100)  NOT NULL,
    [Email] nvarchar(200)  NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Surname] nvarchar(50)  NOT NULL,
    [Birthdate] datetime  NOT NULL,
    [Gender] tinyint  NOT NULL,
    [IsActive] tinyint  NOT NULL,
    [CreationDate] datetime  NOT NULL,
    [Role] int  NOT NULL,
    [Phone] nvarchar(50)  NULL,
    [Height] float  NOT NULL,
    [Weight] float  NOT NULL
);
GO

-- Creating table 'Workouts'
CREATE TABLE [dbo].[Workouts] (
    [ID] uniqueidentifier  NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [VideoID] uniqueidentifier  NULL,
    [Information] nvarchar(max)  NOT NULL,
    [CommentIDs] nvarchar(max)  NULL,
    [Rate] float  NULL,
    [Creator] uniqueidentifier  NOT NULL,
    [Title] nvarchar(200)  NOT NULL,
    [Subtitle] nvarchar(200)  NOT NULL,
    [IsActive] tinyint  NOT NULL
);
GO

-- Creating table 'Comments'
CREATE TABLE [dbo].[Comments] (
    [ID] uniqueidentifier  NOT NULL,
    [Comment] nvarchar(max)  NOT NULL,
    [UserID] uniqueidentifier  NOT NULL,
    [WorkoutID] uniqueidentifier  NULL,
    [LikeCount] int  NULL,
    [ProgramID] uniqueidentifier  NULL
);
GO

-- Creating table 'Rates'
CREATE TABLE [dbo].[Rates] (
    [ID] uniqueidentifier  NOT NULL,
    [Five] int  NOT NULL,
    [Four] int  NOT NULL,
    [Three] int  NOT NULL,
    [Two] int  NOT NULL,
    [One] int  NOT NULL
);
GO

-- Creating table 'Programs'
CREATE TABLE [dbo].[Programs] (
    [ID] uniqueidentifier  NOT NULL,
    [Rate] float  NOT NULL,
    [SelectedWokouts] nvarchar(max)  NOT NULL,
    [Creator] uniqueidentifier  NOT NULL,
    [IsActive] tinyint  NOT NULL,
    [RegisteredUsers] nvarchar(max)  NULL,
    [Title] nvarchar(200)  NOT NULL,
    [Subtitle] nvarchar(200)  NOT NULL,
    [Information] nvarchar(max)  NOT NULL,
    [ViewCount] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'CoreUsers'
ALTER TABLE [dbo].[CoreUsers]
ADD CONSTRAINT [PK_CoreUsers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Sessions'
ALTER TABLE [dbo].[Sessions]
ADD CONSTRAINT [PK_Sessions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [Id] in table 'ErrorLogs'
ALTER TABLE [dbo].[ErrorLogs]
ADD CONSTRAINT [PK_ErrorLogs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ID] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Workouts'
ALTER TABLE [dbo].[Workouts]
ADD CONSTRAINT [PK_Workouts]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [PK_Comments]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Rates'
ALTER TABLE [dbo].[Rates]
ADD CONSTRAINT [PK_Rates]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Programs'
ALTER TABLE [dbo].[Programs]
ADD CONSTRAINT [PK_Programs]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------