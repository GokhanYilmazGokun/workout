//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Workout.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserWeightChangeLog
    {
        public System.Guid ID { get; set; }
        public System.Guid UserId { get; set; }
        public double WeightLoss { get; set; }
        public System.DateTime RecordDate { get; set; }
        public System.Guid ProgramID { get; set; }
        public Nullable<double> FatRatio { get; set; }
        public Nullable<double> MuscleRatio { get; set; }
    }
}
